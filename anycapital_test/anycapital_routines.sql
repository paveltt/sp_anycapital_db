-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `analytics_investment_by_product`
--

DROP TABLE IF EXISTS `analytics_investment_by_product`;
/*!50001 DROP VIEW IF EXISTS `analytics_investment_by_product`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `analytics_investment_by_product` AS SELECT 
 1 AS `product_type_id`,
 1 AS `product_type_name`,
 1 AS `investment_count`,
 1 AS `investment_sum`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `analytics_investment`
--

DROP TABLE IF EXISTS `analytics_investment`;
/*!50001 DROP VIEW IF EXISTS `analytics_investment`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `analytics_investment` AS SELECT 
 1 AS `investment_count`,
 1 AS `investment_sum`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `analytics_deposit`
--

DROP TABLE IF EXISTS `analytics_deposit`;
/*!50001 DROP VIEW IF EXISTS `analytics_deposit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `analytics_deposit` AS SELECT 
 1 AS `ftd_count`,
 1 AS `ftd_sum`,
 1 AS `desposites_count`,
 1 AS `desposites_sum`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `analytics_user`
--

DROP TABLE IF EXISTS `analytics_user`;
/*!50001 DROP VIEW IF EXISTS `analytics_user`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `analytics_user` AS SELECT 
 1 AS `count_users`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `analytics_user_by_campaign`
--

DROP TABLE IF EXISTS `analytics_user_by_campaign`;
/*!50001 DROP VIEW IF EXISTS `analytics_user_by_campaign`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `analytics_user_by_campaign` AS SELECT 
 1 AS `CAMPAIGN_ID`,
 1 AS `CAMPAIGN_NAME`,
 1 AS `count_users`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `analytics_user_by_time`
--

DROP TABLE IF EXISTS `analytics_user_by_time`;
/*!50001 DROP VIEW IF EXISTS `analytics_user_by_time`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `analytics_user_by_time` AS SELECT 
 1 AS `time_of_day`,
 1 AS `count_users`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `analytics_login`
--

DROP TABLE IF EXISTS `analytics_login`;
/*!50001 DROP VIEW IF EXISTS `analytics_login`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `analytics_login` AS SELECT 
 1 AS `count_logins`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `analytics_login_by_time`
--

DROP TABLE IF EXISTS `analytics_login_by_time`;
/*!50001 DROP VIEW IF EXISTS `analytics_login_by_time`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `analytics_login_by_time` AS SELECT 
 1 AS `time_of_day`,
 1 AS `count_logins`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `analytics_investment_by_product`
--

/*!50001 DROP VIEW IF EXISTS `analytics_investment_by_product`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`anycapital`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `analytics_investment_by_product` AS select `p`.`PRODUCT_TYPE_ID` AS `product_type_id`,`pt`.`DISPLAY_NAME` AS `product_type_name`,count(`i`.`ID`) AS `investment_count`,round((sum(`i`.`AMOUNT`) / 100),2) AS `investment_sum` from (((`investments` `i` join `users` `u`) join `products` `p`) join `product_types` `pt`) where ((`i`.`USER_ID` = `u`.`ID`) and (`i`.`PRODUCT_ID` = `p`.`ID`) and (`p`.`PRODUCT_TYPE_ID` = `pt`.`ID`) and (`i`.`INVESTMENT_STATUS_ID` in (2,3)) and (`u`.`CLASS_ID` <> 1) and (cast(`i`.`TIME_CREATED` as date) = curdate())) group by `p`.`PRODUCT_TYPE_ID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `analytics_investment`
--

/*!50001 DROP VIEW IF EXISTS `analytics_investment`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`anycapital`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `analytics_investment` AS select count(`i`.`ID`) AS `investment_count`,round((sum(`i`.`AMOUNT`) / 100),2) AS `investment_sum` from (`investments` `i` join `users` `u`) where ((`i`.`USER_ID` = `u`.`ID`) and (`i`.`INVESTMENT_STATUS_ID` in (2,3)) and (`u`.`CLASS_ID` <> 1) and (cast(`i`.`TIME_CREATED` as date) = curdate())) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `analytics_deposit`
--

/*!50001 DROP VIEW IF EXISTS `analytics_deposit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`anycapital`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `analytics_deposit` AS select count((case when (`ma`.`ID` is not null) then 1 else NULL end)) AS `ftd_count`,round((sum((case when (`ma`.`ID` is not null) then `t`.`AMOUNT` else 0 end)) / 100),2) AS `ftd_sum`,count(`t`.`ID`) AS `desposites_count`,round((sum(`t`.`AMOUNT`) / 100),2) AS `desposites_sum` from (`users` `u` join (`transactions` `t` left join `marketing_attribution` `ma` on(((`ma`.`REFERENCE_ID` = `t`.`ID`) and (`ma`.`TABLE_ID` = 2))))) where ((cast(`t`.`TIME_CREATED` as date) = curdate()) and (`t`.`TRANSACTION_PAYMENT_TYPE_ID` = 1) and (`t`.`TRANSACTION_OPERATION_ID` = 1) and (`u`.`ID` = `t`.`USER_ID`) and (`u`.`CLASS_ID` <> 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `analytics_user`
--

/*!50001 DROP VIEW IF EXISTS `analytics_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`anycapital`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `analytics_user` AS select count(`u`.`ID`) AS `count_users` from `users` `u` where ((cast(`u`.`TIME_CREATED` as date) = curdate()) and (`u`.`CLASS_ID` <> 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `analytics_user_by_campaign`
--

/*!50001 DROP VIEW IF EXISTS `analytics_user_by_campaign`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`anycapital`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `analytics_user_by_campaign` AS select `mt`.`CAMPAIGN_ID` AS `CAMPAIGN_ID`,`mc`.`NAME` AS `CAMPAIGN_NAME`,count(`u`.`ID`) AS `count_users` from (((`users` `u` join `marketing_attribution` `ma`) join `marketing_tracking` `mt`) join `marketing_campaigns` `mc`) where ((`u`.`ID` = `ma`.`REFERENCE_ID`) and (`ma`.`TABLE_ID` = 3) and (`ma`.`TRACKING_ID` = `mt`.`ID`) and (`mt`.`CAMPAIGN_ID` = `mc`.`ID`) and (cast(`u`.`TIME_CREATED` as date) = curdate()) and (`u`.`CLASS_ID` <> 1)) group by `mt`.`CAMPAIGN_ID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `analytics_user_by_time`
--

/*!50001 DROP VIEW IF EXISTS `analytics_user_by_time`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`anycapital`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `analytics_user_by_time` AS select hour(`u`.`TIME_CREATED`) AS `time_of_day`,count(`u`.`ID`) AS `count_users` from `users` `u` where ((cast(`u`.`TIME_CREATED` as date) = curdate()) and (`u`.`CLASS_ID` <> 1)) group by hour(`u`.`TIME_CREATED`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `analytics_login`
--

/*!50001 DROP VIEW IF EXISTS `analytics_login`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`anycapital`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `analytics_login` AS select 1 AS `count_logins` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `analytics_login_by_time`
--

/*!50001 DROP VIEW IF EXISTS `analytics_login_by_time`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`anycapital`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `analytics_login_by_time` AS select 1 AS `time_of_day`,1 AS `count_logins` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:51:27
