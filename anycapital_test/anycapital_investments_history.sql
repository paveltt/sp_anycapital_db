-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `investments_history`
--

DROP TABLE IF EXISTS `investments_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investments_history` (
  `INVESTMNT_ID` bigint(20) NOT NULL,
  `INVESTMENT_STATUS_ID` smallint(6) NOT NULL,
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `WRITER_ID` smallint(6) DEFAULT NULL,
  `ACTION_SOURCE_ID` tinyint(4) NOT NULL,
  `SERVER_NAME` varchar(50) NOT NULL,
  `LOGIN_ID` bigint(20) DEFAULT NULL COMMENT 'the login id of the user that done that action - null if its job/backend',
  `IP` varchar(20) DEFAULT NULL COMMENT 'the  ip of the user that done that action - null if its job',
  PRIMARY KEY (`INVESTMNT_ID`,`INVESTMENT_STATUS_ID`),
  KEY `INVESTMENT_STATUS_ID` (`INVESTMENT_STATUS_ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  KEY `ACTION_SOURCE_ID` (`ACTION_SOURCE_ID`),
  KEY `LOGIN_ID` (`LOGIN_ID`),
  CONSTRAINT `investments_history_ibfk_1` FOREIGN KEY (`INVESTMNT_ID`) REFERENCES `investments` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_history_ibfk_2` FOREIGN KEY (`INVESTMENT_STATUS_ID`) REFERENCES `investment_statuses` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_history_ibfk_3` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_history_ibfk_4` FOREIGN KEY (`ACTION_SOURCE_ID`) REFERENCES `action_source` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_history_ibfk_5` FOREIGN KEY (`LOGIN_ID`) REFERENCES `logins` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investments_history`
--

LOCK TABLES `investments_history` WRITE;
/*!40000 ALTER TABLE `investments_history` DISABLE KEYS */;
INSERT INTO `investments_history` VALUES (10092,2,'2017-06-08 15:21:37.774',NULL,1,'ilww01anycapital.anycapital.com\n',1098,'172.16.2.192'),(10093,2,'2017-06-13 11:16:34.911',NULL,1,'ilww01anycapital.anycapital.com\n',1129,'172.16.2.184'),(10094,2,'2017-06-14 08:29:21.176',NULL,1,'ilww01anycapital.anycapital.com\n',1163,'172.16.2.109'),(10095,1,'2017-06-14 08:29:33.340',NULL,1,'ilww01anycapital.anycapital.com\n',1163,'172.16.2.109'),(10096,2,'2017-06-15 13:12:24.008',NULL,1,'ilww01anycapital.anycapital.com\n',1190,'172.16.9.90'),(10097,2,'2017-06-15 13:36:32.648',NULL,1,'IL-ALEXZ-PC\n',1196,'127.0.0.1'),(10098,2,'2017-06-15 13:38:29.023',NULL,1,'IL-ALEXZ-PC\n',1196,'127.0.0.1'),(10099,2,'2017-06-15 13:38:35.980',NULL,1,'IL-ALEXZ-PC\n',1196,'127.0.0.1'),(10100,2,'2017-06-15 13:38:41.708',NULL,1,'IL-ALEXZ-PC\n',1196,'127.0.0.1'),(10101,2,'2017-06-15 14:16:26.173',NULL,1,'IL-ALEXZ-PC\n',1196,'127.0.0.1'),(10102,2,'2017-06-15 14:16:38.424',NULL,1,'IL-ALEXZ-PC\n',1196,'127.0.0.1'),(10103,2,'2017-06-15 15:24:40.257',NULL,1,'ilww01anycapital.anycapital.com\n',1200,'172.16.2.109'),(10103,3,'2017-06-15 15:24:48.361',NULL,1,'ilww01anycapital.anycapital.com\n',1200,'172.16.2.109'),(10104,2,'2017-06-16 05:29:54.941',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10104,3,'2017-06-16 05:31:43.953',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10105,2,'2017-06-16 05:30:00.392',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10105,3,'2017-06-16 05:31:43.965',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10106,2,'2017-06-16 05:30:05.293',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10106,3,'2017-06-16 05:31:43.976',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10107,2,'2017-06-16 05:30:10.063',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10107,3,'2017-06-16 05:31:44.002',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10108,2,'2017-06-16 05:30:14.799',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10108,3,'2017-06-16 05:31:44.039',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10109,2,'2017-06-16 05:30:21.561',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10109,3,'2017-06-16 05:31:44.050',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10110,2,'2017-06-16 05:32:59.865',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10111,2,'2017-06-16 05:33:05.813',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10112,2,'2017-06-16 05:33:11.358',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10112,3,'2017-06-16 05:33:58.513',NULL,1,'ilww01anycapital.anycapital.com\n',1202,'172.16.2.109'),(10113,2,'2017-06-26 09:53:31.897',NULL,1,'ilww01anycapital.anycapital.com\n',1289,'172.16.2.96'),(10114,2,'2017-06-26 10:02:38.113',NULL,1,'ilww01anycapital.anycapital.com\n',1289,'172.16.2.96'),(10115,2,'2017-07-03 09:13:30.358',NULL,1,'ilww01anycapital.anycapital.com\n',1299,'172.16.2.88'),(10115,3,'2017-07-03 11:47:34.950',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10116,2,'2017-07-03 12:15:43.087',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10116,3,'2017-07-03 12:54:22.086',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10117,2,'2017-07-03 12:18:12.336',NULL,1,'ilww01anycapital.anycapital.com\n',1301,'172.16.2.96'),(10117,3,'2017-07-03 13:21:51.343',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10118,2,'2017-07-03 12:46:25.351',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10118,3,'2017-07-03 13:21:51.363',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10119,2,'2017-07-03 12:53:35.836',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10119,3,'2017-07-03 13:21:51.386',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10120,2,'2017-07-03 13:23:26.861',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10121,2,'2017-07-03 13:28:32.528',NULL,1,'ilww01anycapital.anycapital.com\n',1300,'172.16.2.88'),(10121,3,'2017-07-04 14:18:51.776',NULL,1,'ilww01anycapital.anycapital.com\n',1306,'172.16.2.88'),(10122,2,'2017-07-04 06:31:42.376',NULL,1,'ilww01anycapital.anycapital.com\n',1302,'172.16.2.88'),(10122,3,'2017-07-31 15:08:18.992',NULL,1,'ilww01anycapital.anycapital.com\n',1468,'199.203.251.26'),(10123,2,'2017-07-05 09:18:41.726',NULL,1,'ilww01anycapital.anycapital.com\n',1307,'172.16.2.96'),(10123,3,'2017-07-05 09:22:55.105',NULL,1,'ilww01anycapital.anycapital.com\n',1307,'172.16.2.96'),(10124,2,'2017-07-05 09:23:51.199',NULL,1,'ilww01anycapital.anycapital.com\n',1307,'172.16.2.96'),(10125,2,'2017-07-05 09:24:21.659',NULL,1,'ilww01anycapital.anycapital.com\n',1307,'172.16.2.96'),(10126,2,'2017-07-05 09:40:14.494',NULL,1,'ilww01anycapital.anycapital.com\n',1307,'172.16.2.96'),(10127,2,'2017-07-05 13:21:34.812',NULL,1,'ilww01anycapital.anycapital.com\n',1309,'172.16.2.96'),(10128,1,'2017-07-05 13:23:47.663',NULL,1,'ilww01anycapital.anycapital.com\n',1310,'172.16.2.96'),(10128,5,'2017-07-08 00:00:05.139',NULL,3,'ilww01anycapital.anycapital.com\n',NULL,NULL),(10129,1,'2017-07-05 13:39:28.001',NULL,1,'ilww01anycapital.anycapital.com\n',1310,'172.16.2.96'),(10129,5,'2017-07-08 00:00:05.094',NULL,3,'ilww01anycapital.anycapital.com\n',NULL,NULL),(10130,2,'2017-07-09 07:11:25.309',NULL,1,'ilww01anycapital.anycapital.com\n',1312,'172.16.2.221'),(10131,2,'2017-07-09 09:05:19.458',NULL,1,'ilww01anycapital.anycapital.com\n',1314,'199.203.251.26'),(10132,2,'2017-07-09 09:08:05.925',NULL,1,'ilww01anycapital.anycapital.com\n',1314,'199.203.251.26'),(10133,1,'2017-07-09 09:13:59.299',NULL,1,'ilww01anycapital.anycapital.com\n',1314,'199.203.251.26'),(10133,5,'2017-07-12 00:00:05.021',NULL,3,'ilww01anycapital.anycapital.com\n',NULL,NULL),(10134,2,'2017-07-10 07:42:24.392',NULL,1,'bg-pavlinm-pc\n',1318,'127.0.0.1'),(10135,2,'2017-07-10 07:43:50.970',NULL,1,'bg-pavlinm-pc\n',1318,'127.0.0.1'),(10136,2,'2017-07-10 08:03:30.583',NULL,1,'bg-pavlinm-pc\n',1318,'127.0.0.1'),(10137,2,'2017-07-10 08:13:59.939',NULL,1,'bg-pavlinm-pc\n',1318,'127.0.0.1'),(10138,2,'2017-07-10 08:18:22.792',NULL,1,'bg-pavlinm-pc\n',1318,'127.0.0.1'),(10139,2,'2017-07-11 14:19:16.629',NULL,1,'ilww01anycapital.anycapital.com\n',1326,'199.203.251.26'),(10140,2,'2017-07-11 14:29:56.182',NULL,1,'ilww01anycapital.anycapital.com\n',1326,'199.203.251.26'),(10141,2,'2017-07-12 07:43:32.116',NULL,1,'ilww01anycapital.anycapital.com\n',1349,'199.203.251.26'),(10142,1,'2017-07-12 11:03:14.730',NULL,1,'ilww01anycapital.anycapital.com\n',1357,'93.109.212.202'),(10142,5,'2017-07-14 00:00:05.031',NULL,3,'ilww01anycapital.anycapital.com\n',NULL,NULL),(10143,2,'2017-07-13 07:44:54.476',NULL,1,'ilww01anycapital.anycapital.com\n',1361,'217.12.196.142'),(10144,2,'2017-07-23 09:38:25.316',NULL,1,'ilww01anycapital.anycapital.com\n',1397,'199.203.251.26'),(10145,2,'2017-07-25 09:10:23.410',NULL,1,'ilww01anycapital.anycapital.com\n',1399,'199.203.251.26'),(10148,2,'2017-07-30 12:19:33.800',NULL,1,'IL-EYALO-PC\n',NULL,'127.0.0.1'),(10149,2,'2017-07-30 12:32:27.021',NULL,1,'IL-EYALO-PC\n',NULL,'127.0.0.1'),(10150,2,'2017-07-30 12:36:54.931',NULL,1,'IL-EYALO-PC\n',NULL,'127.0.0.1'),(10151,2,'2017-07-30 12:52:33.146',NULL,1,'IL-EYALO-PC\n',NULL,'127.0.0.1'),(10152,1,'2017-07-30 13:22:03.655',NULL,1,'IL-EYALO-PC\n',NULL,'127.0.0.1'),(10152,5,'2017-08-04 00:00:05.097',NULL,3,'ilww01anycapital.anycapital.com\n',NULL,NULL),(10153,2,'2017-07-30 13:26:19.926',NULL,1,'IL-EYALO-PC\n',NULL,'127.0.0.1'),(10155,1,'2017-07-31 10:26:12.319',NULL,1,'ilww01anycapital.anycapital.com\n',1463,'199.203.251.26'),(10155,2,'2017-07-31 10:30:31.515',3,2,'ilww01anycapital.anycapital.com\n',NULL,'199.203.251.26'),(10159,2,'2017-07-31 15:08:35.747',NULL,1,'ilww01anycapital.anycapital.com\n',1468,'199.203.251.26'),(10160,2,'2017-07-31 15:10:29.120',NULL,1,'ilww01anycapital.anycapital.com\n',1468,'199.203.251.26'),(10160,3,'2017-07-31 15:10:47.274',NULL,1,'ilww01anycapital.anycapital.com\n',1468,'199.203.251.26'),(10161,2,'2017-07-31 15:10:54.880',NULL,1,'ilww01anycapital.anycapital.com\n',1468,'199.203.251.26'),(10162,2,'2017-08-20 11:19:11.662',NULL,1,'ilww01anycapital.anycapital.com\n',1525,'199.203.251.26'),(10163,2,'2017-08-20 11:19:27.414',NULL,1,'ilww01anycapital.anycapital.com\n',1525,'199.203.251.26'),(10214,1,'2017-08-20 13:19:29.770',NULL,1,'ilww01anycapital.anycapital.com\n',1534,'199.203.251.26'),(10226,2,'2017-08-22 09:28:03.481',NULL,1,'IL-ALEXZ-PC\n',1539,'0:0:0:0:0:0:0:1'),(10226,3,'2017-12-04 10:18:57.318',NULL,1,'wwtest00.anycapital.com\n',1719,'92.247.82.234'),(10260,2,'2017-12-04 10:19:40.818',NULL,1,'wwtest00.anycapital.com\n',1719,'92.247.82.234');
/*!40000 ALTER TABLE `investments_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:50:26
