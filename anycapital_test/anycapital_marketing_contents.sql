-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marketing_contents`
--

DROP TABLE IF EXISTS `marketing_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketing_contents` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Content''s id',
  `NAME` varchar(50) NOT NULL COMMENT 'Content''s name',
  `COMMENTS` varchar(4000) DEFAULT NULL COMMENT 'Any comments',
  `PAGE_CONTENT` longtext COMMENT 'Page content can include html file etc.',
  `WRITER_ID` smallint(6) NOT NULL COMMENT 'Foreign key to WRITERS',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'Content''s time created',
  `TIME_MODIFIED` timestamp NULL DEFAULT NULL COMMENT 'Content''s time updated',
  `HTML_FILE_PATH` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  CONSTRAINT `marketing_contents_ibfk_1` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_contents`
--

LOCK TABLES `marketing_contents` WRITE;
/*!40000 ALTER TABLE `marketing_contents` DISABLE KEYS */;
INSERT INTO `marketing_contents` VALUES (1,'default','',NULL,3,'2017-02-11 20:38:26.000','2017-08-26 17:06:00',NULL),(21,'emails-cm1',NULL,NULL,3,'2017-08-26 17:19:19.619','2017-10-24 15:13:22','21_-_aaa.html'),(52,'anycapital_mailer_v_a1',NULL,NULL,3,'2017-10-15 07:55:59.167','2017-10-15 07:56:46','52_-_anycapital_mailer_v_a1.html'),(53,'anycapital_mailer_v_b',NULL,NULL,3,'2017-10-15 07:57:19.746',NULL,'53_-_anycapital_mailer_v_b.html');
/*!40000 ALTER TABLE `marketing_contents` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:51:00
