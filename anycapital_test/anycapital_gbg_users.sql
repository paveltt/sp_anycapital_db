-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gbg_users`
--

DROP TABLE IF EXISTS `gbg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gbg_users` (
  `user_id` bigint(20) NOT NULL,
  `score` int(11) NOT NULL,
  `request` longtext,
  `response` longtext,
  `time_created` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `time_updated` timestamp(3) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_gbg_users_users_idx` (`user_id`),
  CONSTRAINT `fk_gbg_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gbg_users`
--

LOCK TABLES `gbg_users` WRITE;
/*!40000 ALTER TABLE `gbg_users` DISABLE KEYS */;
INSERT INTO `gbg_users` VALUES (10286,3232,'<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://www.id3global.com/ID3gWS/2013/04\"><SOAP-ENV:Header xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\" SOAP-ENV:mustUnderstand=\"1\"><wsse:UsernameToken><wsse:Username>admin@anyoption.com</wsse:Username><wsse:Password>@n1Option1234567</wsse:Password></wsse:UsernameToken></wsse:Security></SOAP-ENV:Header><SOAP-ENV:Body><ns:AuthenticateSP><ns:ProfileIDVersion><ns:ID>89d8add5-53eb-4695-816a-9a657e60ff8e</ns:ID><ns:Version>0</ns:Version></ns:ProfileIDVersion><ns:InputData><ns:Personal><ns:PersonalDetails><ns:Forename>Giuseppe</ns:Forename><ns:Surname>Valente</ns:Surname><ns:Gender>Male</ns:Gender><ns:DOBDay>02</ns:DOBDay><ns:DOBMonth>06</ns:DOBMonth><ns:DOBYear>1979</ns:DOBYear></ns:PersonalDetails></ns:Personal><ns:Addresses><ns:CurrentAddress><ns:Country>Germany</ns:Country><ns:Street>rossmarkt 11</ns:Street><ns:AddressLine1>rossmarkt 11 55232</ns:AddressLine1><ns:City>11</ns:City><ns:ZipPostcode>alzey</ns:ZipPostcode><ns:Building>55232</ns:Building></ns:CurrentAddress></ns:Addresses></ns:InputData></ns:AuthenticateSP></SOAP-ENV:Body></SOAP-ENV:Envelope>','<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:u=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><s:Header><o:Security s:mustUnderstand=\"1\" xmlns:o=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"><u:Timestamp u:Id=\"_0\"><u:Created>2017-04-26T08:11:19.322Z</u:Created><u:Expires>2017-04-26T08:16:19.322Z</u:Expires></u:Timestamp></o:Security></s:Header><s:Body><AuthenticateSPResponse xmlns=\"http://www.id3global.com/ID3gWS/2013/04\"><AuthenticateSPResult xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"><AuthenticationID>231f5ace-3b33-4b87-9010-f2533fd3162d</AuthenticationID><Timestamp>2017-04-26T09:11:18.9179364+01:00</Timestamp><ProfileID>89d8add5-53eb-4695-816a-9a657e60ff8e</ProfileID><ProfileName>Germany</ProfileName><ProfileVersion>1</ProfileVersion><ProfileRevision>2</ProfileRevision><ProfileState>Effective</ProfileState><ResultCodes><GlobalItemCheckResultCodes><Name>Germany Financial</Name><Description>Germany Financial Database check.  Performs authentication of first, last name, date of birth, address and telephone against a combined credit, telephone and marketing data records.</Description><Comment><GlobalItemCheckResultCode><Description>No/Insufficient telephone number supplied</Description><Code>260</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>No/insufficient mobile telephone supplied</Description><Code>450</Code></GlobalItemCheckResultCode></Comment><Match><GlobalItemCheckResultCode><Description>Country valid/matched</Description><Code>2265</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Surname valid/matched</Description><Code>2101</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Date of birth valid/matched</Description><Code>2111</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Gender valid/matched</Description><Code>2104</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Address valid/matched</Description><Code>2260</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Building valid/matched</Description><Code>2262</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Street valid/matched</Description><Code>2263</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Zip/Postcode valid/matched</Description><Code>2266</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Forename, Surname and Address matched</Description><Code>3501</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Forename, Surname and Date of birth matched</Description><Code>3502</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Forename, Surname, Address and Date of birth matched</Description><Code>3503</Code></GlobalItemCheckResultCode></Match><Warning><GlobalItemCheckResultCode><Description>Partial forename matched</Description><Code>4100</Code></GlobalItemCheckResultCode><GlobalItemCheckResultCode><Description>Source indicates that the address is a non-residential address.</Description><Code>6502</Code></GlobalItemCheckResultCode></Warning><Mismatch><GlobalItemCheckResultCode><Description>Locality invalid/did not match</Description><Code>8264</Code></GlobalItemCheckResultCode></Mismatch><ID>251</ID><Pass>Nomatch</Pass><Address>Match</Address><Forename>Nomatch</Forename><Surname>Match</Surname><DOB>Match</DOB><Alert>Nomatch</Alert></GlobalItemCheckResultCodes><GlobalItemCheckResultCodes><Name>Germany Landline</Name><Description>Germany Landline Database check.  Performs authentication of first, last name, address and telephone information against landline telephone records.</Description><Comment><GlobalItemCheckResultCode><Description>This result code is no longer supported. (No/Insufficient address supplied)</Description><Code>410</Code></GlobalItemCheckResultCode></Comment><ID>203</ID><Pass>Nomatch</Pass><Address>Nomatch</Address><Forename>Nomatch</Forename><Surname>Nomatch</Surname><DOB>NA</DOB><Alert>NA</Alert></GlobalItemCheckResultCodes><GlobalItemCheckResultCodes><Name>Germany Consumer</Name><Description>Germany Consumer Database check.  Performs authentication of first, last name and address against a consumer marketing database.</Description><Comment><GlobalItemCheckResultCode><Description>This result code is no longer supported. (No/Insufficient address supplied)</Description><Code>410</Code></GlobalItemCheckResultCode></Comment><ID>201</ID><Pass>Nomatch</Pass><Address>Nomatch</Address><Forename>Nomatch</Forename><Surname>Nomatch</Surname><DOB>NA</DOB><Alert>NA</Alert></GlobalItemCheckResultCodes><GlobalItemCheckResultCodes><Name>Germany Postal</Name><Description>Germany Postal Database check.  Performs authentication of first, last name, address and telephone information against the postal records. </Description><Comment><GlobalItemCheckResultCode><Description>This result code is no longer supported. (No/Insufficient address supplied)</Description><Code>410</Code></GlobalItemCheckResultCode></Comment><ID>202</ID><Pass>Nomatch</Pass><Address>Nomatch</Address><Forename>Nomatch</Forename><Surname>Nomatch</Surname><DOB>NA</DOB><Alert>NA</Alert></GlobalItemCheckResultCodes><GlobalItemCheckResultCodes><Name>Germany Mobile</Name><Description>Germany Mobile Database check.  Performs authentication of first, last name, address and telephone information against mobile telephone records.</Description><Comment><GlobalItemCheckResultCode><Description>This result code is no longer supported. (No/Insufficient address supplied)</Description><Code>410</Code></GlobalItemCheckResultCode></Comment><ID>204</ID><Pass>Nomatch</Pass><Address>Nomatch</Address><Forename>Nomatch</Forename><Surname>Nomatch</Surname><DOB>NA</DOB><Alert>NA</Alert></GlobalItemCheckResultCodes><GlobalItemCheckResultCodes i:type=\"GlobalSanctionsResultCodes\"><Name>International PEP (Enhanced)</Name><Description>International PEP (Enhanced) Database check.  Provides authentication against Politically Exposed Persons lists from across the globe (contains known associates and known alias details)</Description><Match><GlobalItemCheckResultCode><Description>Supplied full name did not match.</Description><Code>3500</Code></GlobalItemCheckResultCode></Match><ID>209</ID><Pass>NA</Pass><Address>NA</Address><Forename>Nomatch</Forename><Surname>Nomatch</Surname><DOB>Nomatch</DOB><Alert>Nomatch</Alert><SanctionsMatches/></GlobalItemCheckResultCodes><GlobalItemCheckResultCodes i:type=\"GlobalSanctionsResultCodes\"><Name>International Sanctions (Enhanced)</Name><Description>International Sanctions (Enhanced) check.  Provides authentication against multiple Sanctions and Enforcement lists across the globe (lists are selectable at profile level)</Description><Match><GlobalItemCheckResultCode><Description>Supplied full name did not match.</Description><Code>3500</Code></GlobalItemCheckResultCode></Match><ID>208</ID><Pass>NA</Pass><Address>Nomatch</Address><Forename>Nomatch</Forename><Surname>Nomatch</Surname><DOB>Nomatch</DOB><Alert>Nomatch</Alert><SanctionsMatches/></GlobalItemCheckResultCodes></ResultCodes><Score>3232</Score><BandText>Pass - 2 x Name, Address and DOB Match</BandText><Country>Germany</Country></AuthenticateSPResult></AuthenticateSPResponse></s:Body></s:Envelope>','2017-04-26 08:11:19.346','2017-04-26 08:11:19.346');
/*!40000 ALTER TABLE `gbg_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:54
