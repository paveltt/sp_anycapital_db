-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_barriers`
--

DROP TABLE IF EXISTS `product_barriers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_barriers` (
  `PRODUCT_ID` bigint(20) NOT NULL,
  `IS_BARRIER_OCCUR` binary(1) NOT NULL DEFAULT '0' COMMENT '1 if a barrier event has  occur else 0',
  `BARRIER_LEVEL` double DEFAULT NULL COMMENT 'percentage of the market price at the start of the product to touch the barrier',
  `PRODUCT_BARRIER_TYPE_ID` smallint(6) NOT NULL,
  `BARRIER_END` timestamp NULL DEFAULT NULL COMMENT 'end date to check barrier',
  `BARRIER_START` timestamp NULL DEFAULT NULL COMMENT 'start date to check barrier',
  PRIMARY KEY (`PRODUCT_ID`),
  KEY `PRODUCT_BARRIER_TYPE_ID` (`PRODUCT_BARRIER_TYPE_ID`),
  CONSTRAINT `product_barriers_ibfk_1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `product_barriers_ibfk_2` FOREIGN KEY (`PRODUCT_BARRIER_TYPE_ID`) REFERENCES `product_barrier_types` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_barriers`
--

LOCK TABLES `product_barriers` WRITE;
/*!40000 ALTER TABLE `product_barriers` DISABLE KEYS */;
INSERT INTO `product_barriers` VALUES (10134,'0',200,1,'2017-06-09 03:00:00','2017-06-08 03:00:00'),(10135,'0',130,1,'2018-07-10 00:00:00','2017-07-10 00:00:00'),(10137,'0',222,1,'2017-06-10 03:00:00','2017-06-09 03:00:00'),(10138,'0',222,1,'2017-06-10 00:00:00','2017-06-09 00:00:00'),(10139,'0',30,1,'2017-07-09 09:00:00','2017-06-09 09:00:00'),(10142,'0',130,1,'2019-07-06 00:00:00','2018-07-10 00:00:00'),(10143,'0',222,1,'2017-06-10 06:00:00','2017-06-09 06:00:00'),(10144,'0',222,1,'2017-06-10 06:00:00','2017-06-09 06:00:00'),(10145,'0',222,1,'2017-06-10 03:00:00','2017-06-09 03:00:00'),(10146,'0',222,1,'2017-06-10 00:00:00','2017-06-09 00:00:00'),(10147,'0',222,1,'2017-06-10 00:00:00','2017-06-09 00:00:00'),(10148,'0',222,1,'2017-06-10 03:00:00','2017-06-09 03:00:00'),(10149,'0',222,1,'2017-06-13 00:00:00','2017-06-12 00:00:00'),(10151,'0',222,1,'2017-06-14 03:00:00','2017-06-13 03:00:00'),(10152,'0',222,1,'2018-06-14 00:00:00','2018-06-13 00:00:00'),(10153,'0',222,1,'2017-06-14 06:00:00','2017-06-13 06:00:00'),(10154,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10155,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10156,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10157,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10158,'0',222,1,'2017-06-14 03:00:00','2017-06-13 03:00:00'),(10159,'0',222,1,'2017-06-14 03:00:00','2017-06-13 03:00:00'),(10160,'0',222,1,'2017-06-14 03:00:00','2017-06-13 03:00:00'),(10161,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10164,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10165,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10166,'0',222,1,'2017-06-14 03:00:00','2017-06-13 03:00:00'),(10167,'0',222,1,'2017-06-15 03:00:00','2017-06-14 03:00:00'),(10168,'0',65,1,'2020-07-13 00:00:00','2017-07-13 00:00:00'),(10169,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10170,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10171,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10172,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10173,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10174,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10175,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10176,'0',222,1,'2017-06-14 00:00:00','2017-06-13 00:00:00'),(10177,'0',222,1,'2017-06-23 00:00:00','2017-06-22 00:00:00'),(10178,'0',333,1,'2017-06-24 00:00:00','2017-06-23 00:00:00'),(10179,'0',60,2,'2018-07-13 00:00:00','2017-07-14 00:00:00'),(10182,'0',70,1,'2018-08-03 00:00:00','2017-08-04 00:00:00'),(10183,'0',70,1,'2018-08-03 00:00:00','2017-08-04 00:00:00'),(10185,'0',60,1,'2018-08-23 00:00:00','2017-08-23 00:00:00'),(10186,'0',60,1,'2018-08-23 00:00:00','2017-08-23 00:00:00'),(10187,'0',60,1,'2018-08-23 00:00:00','2017-08-23 00:00:00'),(10188,'0',111,1,'2018-05-01 00:00:00','2017-12-02 00:00:00');
/*!40000 ALTER TABLE `product_barriers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:34
