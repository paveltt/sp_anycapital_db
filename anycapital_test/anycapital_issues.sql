-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `issues`
--

DROP TABLE IF EXISTS `issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'The issue''s id.',
  `channel_id` smallint(6) NOT NULL COMMENT 'Foreign key to issue_channels',
  `subject_id` smallint(6) NOT NULL COMMENT 'Foreign key to issue_subjects',
  `direction_id` smallint(6) DEFAULT NULL COMMENT 'Foreign key to issue_directions',
  `reached_status_id` smallint(6) DEFAULT NULL COMMENT 'Foreign key to issue_reached_statuses',
  `reaction_id` smallint(6) DEFAULT NULL COMMENT 'Foreign key to issue_reactions',
  `comments` varchar(2000) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Any comment regarding to the issue.',
  `significant_note` binary(1) NOT NULL COMMENT 'Represent if it''s significant note or not.',
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The record''s time created.',
  `user_id` bigint(20) NOT NULL COMMENT 'Foreign key to USERS.',
  `writer_id` smallint(6) NOT NULL COMMENT 'Foreign key to writers',
  `table_id` smallint(6) DEFAULT NULL,
  `reference_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `issues_fk_1_idx` (`channel_id`),
  KEY `issues_fk_2_idx` (`subject_id`),
  KEY `issues_fk_3_idx` (`direction_id`),
  KEY `issues_fk_4_idx` (`reached_status_id`),
  KEY `issues_fk_5_idx` (`reaction_id`),
  KEY `issues_fk_6_idx` (`user_id`),
  KEY `issues_fk_7_idx` (`writer_id`),
  KEY `issues_fk_8_idx` (`user_id`),
  KEY `issues_fk_9_idx` (`writer_id`),
  KEY `issues_fk_10_idx` (`user_id`),
  KEY `issues_fk_11_idx` (`writer_id`),
  KEY `issues_fk_6_idx1` (`table_id`),
  CONSTRAINT `issues_fk_6` FOREIGN KEY (`table_id`) REFERENCES `tables` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_1` FOREIGN KEY (`channel_id`) REFERENCES `issue_channels` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_2` FOREIGN KEY (`subject_id`) REFERENCES `issue_subjects` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_3` FOREIGN KEY (`direction_id`) REFERENCES `issue_directions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_4` FOREIGN KEY (`reached_status_id`) REFERENCES `issue_reached_statuses` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_5` FOREIGN KEY (`reaction_id`) REFERENCES `issue_reactions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issues`
--

LOCK TABLES `issues` WRITE;
/*!40000 ALTER TABLE `issues` DISABLE KEYS */;
INSERT INTO `issues` VALUES (2,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,0,3,10103),(3,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,0,3,10103),(4,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(5,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(6,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(7,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(8,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(9,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(10,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(11,3,1,2,2,3,'test','1','2017-11-09 13:12:22',0,0,3,0),(12,3,1,2,2,1,'test mira','1','2017-11-09 13:12:22',0,0,3,0),(13,3,1,2,2,3,'test','1','2017-11-09 13:12:22',10103,0,3,10103),(14,3,1,2,2,1,'test mira','1','2017-11-09 13:12:22',10103,0,3,10103),(18,3,1,1,2,3,'test','1','2017-11-09 13:12:22',10103,0,3,10103),(19,3,1,2,1,3,'testttt','1','2017-11-09 13:12:22',10103,0,3,10103),(20,3,1,2,2,1,'test mira','1','2017-11-09 13:12:22',10103,21,3,10103),(24,1,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(30,1,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(34,2,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(35,2,1,NULL,NULL,NULL,'not call','1','2017-11-09 13:12:22',10103,21,3,10103),(36,2,1,NULL,NULL,NULL,'not call','1','2017-11-09 13:12:22',10105,21,3,10105),(37,3,1,2,2,1,'mira test','1','2017-11-09 13:12:22',10103,21,3,10103),(38,2,1,NULL,NULL,NULL,'mira test','0','2017-11-09 13:12:22',10103,21,3,10103),(39,2,1,NULL,NULL,NULL,'not call','1','2017-11-09 13:12:22',10103,21,3,10103),(40,4,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(41,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(42,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(43,2,1,NULL,NULL,NULL,'test','0','2017-11-09 13:12:22',10103,21,3,10103),(44,2,1,NULL,NULL,NULL,'test2','0','2017-11-09 13:12:22',10103,21,3,10103),(45,2,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(46,2,1,NULL,NULL,NULL,'test3','0','2017-11-09 13:12:22',10103,21,3,10103),(47,3,1,2,1,5,'test4','1','2017-11-09 13:12:22',10103,21,3,10103),(48,2,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(53,1,1,1,1,NULL,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(55,2,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(58,3,1,2,2,4,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(59,3,1,1,1,2,'test mira','0','2017-11-09 13:12:22',10103,21,3,10103),(62,3,1,2,1,1,NULL,'0','2017-11-09 13:12:22',0,41,3,0),(64,3,1,2,1,2,'test reaction','0','2017-11-09 13:12:22',10103,21,3,10103),(65,3,1,2,2,4,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(66,2,1,NULL,NULL,NULL,'test nulls','1','2017-11-09 13:12:22',10103,21,3,10103),(67,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(68,3,1,2,2,3,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(69,2,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(70,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(71,2,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(72,3,1,2,2,4,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(73,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(74,2,1,NULL,NULL,NULL,'last test','1','2017-11-09 13:12:22',10103,21,3,10103),(75,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(76,2,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(77,2,1,NULL,NULL,NULL,'test','1','2017-11-09 13:12:22',10103,21,3,10103),(78,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(79,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(80,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(81,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(82,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',10103,21,3,10103),(83,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',0,21,3,0),(84,2,1,NULL,NULL,NULL,NULL,'0','2017-11-09 13:12:22',0,21,3,0),(85,2,1,NULL,NULL,NULL,'test','0','2017-11-09 13:12:22',10124,3,3,10124),(86,2,1,NULL,NULL,NULL,'dfsdf','0','2017-11-09 13:12:22',1,22,3,1),(87,3,1,2,2,3,'fdgdg','0','2017-11-09 13:12:22',1,22,3,1),(88,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(89,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(90,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(91,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(92,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(93,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(94,3,1,1,2,3,NULL,'0','2017-11-09 13:12:22',10177,3,3,10177),(95,3,1,1,1,1,'ggf','0','2017-11-09 13:12:22',10177,3,3,10177),(96,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(97,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10103,6,3,10103),(98,1,1,1,1,1,'test','1','2017-11-09 13:12:22',10656,-1,3,10656),(99,3,1,2,1,1,'I explained products to client. He is very interested','1','2017-11-09 13:12:22',10669,108,3,10669),(100,3,1,1,2,3,'The user didn\'t answer to the phone number which we got','0','2017-11-09 13:12:22',10124,3,3,10124);
/*!40000 ALTER TABLE `issues` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:48:18
