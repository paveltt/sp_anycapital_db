-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `LAST_NAME` varchar(100) DEFAULT NULL,
  `TYPE_ID` tinyint(4) NOT NULL,
  `MOBILE_PHONE` varchar(30) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `COUNTRY_ID` smallint(6) NOT NULL,
  `LANGUAGE_ID` smallint(6) NOT NULL,
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `TIME_MODIFIED` timestamp NULL DEFAULT NULL,
  `WRITER_ID` smallint(6) DEFAULT NULL,
  `UTC_OFFSET` smallint(6) DEFAULT NULL,
  `IP` varchar(20) DEFAULT NULL,
  `USER_AGENT` varchar(2000) DEFAULT NULL,
  `IS_CONTACT_BY_EMAIL` binary(1) DEFAULT '1',
  `IS_CONTACT_BY_SMS` binary(1) DEFAULT '1',
  `CLASS_ID` tinyint(4) DEFAULT '1',
  `ACTION_SOURCE_ID` tinyint(4) NOT NULL,
  `HTTP_REFERER` varchar(4000) DEFAULT NULL,
  `ISSUE_ID` tinyint(4) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `MARKETING_TRACKING_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ISSUE_ID` (`ISSUE_ID`),
  KEY `COUNTRY_ID` (`COUNTRY_ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  KEY `CLASS_ID` (`CLASS_ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  KEY `ACTION_SOURCE_ID` (`ACTION_SOURCE_ID`),
  KEY `contacts_ibfk_7_idx` (`LANGUAGE_ID`),
  KEY `contacts_ibfk_7_idx1` (`MARKETING_TRACKING_ID`),
  CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`ISSUE_ID`) REFERENCES `contact_issues` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `countries` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_3` FOREIGN KEY (`TYPE_ID`) REFERENCES `contact_types` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_4` FOREIGN KEY (`CLASS_ID`) REFERENCES `user_classes` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_5` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_6` FOREIGN KEY (`ACTION_SOURCE_ID`) REFERENCES `action_source` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_7` FOREIGN KEY (`LANGUAGE_ID`) REFERENCES `languages` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (2,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:44:56.083',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(3,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:45:07.014',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(4,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:45:15.756',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(5,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:45:33.253',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(6,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:45:50.669',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(7,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:46:02.650',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(8,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:46:07.484',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(9,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:46:17.932',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(10,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:46:32.437',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(11,'test','test',2,'12345678','test2@test.com',2,2,'2017-05-18 12:46:40.445',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,1,'test!!!!',NULL),(12,'test','test',2,'123123123','test@anyoption.com',226,2,'2017-05-19 07:24:08.657',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,5,'test',NULL),(13,'435345345345345','435435345345435',2,'1','dsfsdfdsf@fg.vb',226,2,'2017-05-21 12:46:38.722',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','0','0',1,1,NULL,2,'2',NULL),(14,'test','test',2,'12345678','230517@anycapital.com',226,2,'2017-05-23 09:13:27.353',NULL,NULL,0,'127.0.0.1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36','0','0',1,1,NULL,2,'test',NULL),(15,'kkk','kkk',2,'1111111','kkk@kkk.kkk',226,2,'2017-06-01 10:13:22.900',NULL,NULL,0,'172.16.2.184','Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0','1','1',1,1,NULL,5,'kkk2111',NULL),(16,'asdasd','asdsadas',2,'324324234','asdasd@asd.com',226,2,'2017-06-01 10:14:30.313',NULL,NULL,0,'172.16.2.184','Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0','1','1',1,1,NULL,4,'sadddddddddddddddddddddddddddddddd\nasddddddddddddddddddddddddddd\nacxzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz\nx\nx\nx\nf\nsdaf\ndsf\nsdf\nsdf\ndsf\ndsf',NULL),(17,'asdad','a',2,'23423423','reg2711@test.com',226,2,'2017-06-01 10:16:07.377',NULL,NULL,0,'172.16.2.184','Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0','1','1',1,1,NULL,5,'q\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nqq\nq\nq\nq\nq\nq\nq\nq\nq\nq\nq',NULL),(18,'dfd','dfs',2,'33333333333333333333','dsfsd@sad.ee',226,2,'2017-06-04 06:44:22.639',NULL,NULL,0,'0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','1','1',1,1,NULL,2,'1324',NULL),(19,'mnh','fdgbfh',2,'23312443434444444444','hgfh@sadsd.dd',226,2,'2017-06-05 07:57:41.344',NULL,NULL,0,'0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','1','1',1,1,NULL,2,'dsafdfsdfsddfdsaffvsdgvfdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsdsvdfsvvvvvvvdsfdsafsdafdsdsdsdsdsdsdsdsdsdsdsdsaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',NULL),(20,'asdasd','asdsad',2,'324324234234','sadasdas@sdfsdf.comn',226,2,'2017-06-06 07:03:49.620',NULL,NULL,0,'172.16.2.184','Mozilla/5.0 (Windows NT 6.3; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0','1','1',1,1,NULL,3,'s\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\n\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns\ns',NULL),(21,'test','te',2,'1234567','test@test.com',155,2,'2017-06-19 15:09:12.796',NULL,NULL,0,'217.12.196.142','Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36','1','1',1,1,NULL,2,'sdfdsafsa',NULL),(22,'sdf','sdfsd',2,'1234567','df@sd.ss',155,2,'2017-06-19 15:12:34.280',NULL,NULL,0,'217.12.196.142','Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36','1','1',1,1,NULL,2,'sdfsdf',NULL),(23,'Ejfjrkm','Gugfjhdjk',2,'76588888','cdikn8jfyj@gmail.com',226,2,'2017-07-02 09:45:14.047',NULL,NULL,0,'172.16.9.31','Mozilla/5.0 (Linux; Android 6.0.1; SM-G900F Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36','1','1',1,1,NULL,5,'Y',NULL),(24,'ts','dsg',2,'2452521','sgshr3r42@gmail.com',226,2,'2017-07-04 11:55:22.214',NULL,NULL,0,'172.16.2.88','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','1','1',1,1,NULL,5,'33',NULL),(32,'michael','paul',2,'99527572','michael.paul@anyoption.com',163,2,'2017-07-26 06:48:41.892',NULL,NULL,0,'195.14.156.133','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36','1','1',1,1,NULL,2,'This is a test.\n\nWhere is this being received?\n\nThanks',NULL),(33,'test','test',2,'1231231','eyal.o23102017A@invest.com',226,2,'2017-10-23 07:00:41.281',NULL,NULL,0,'0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36','1','1',1,1,NULL,2,'test',NULL),(34,'test','test',2,'12345637','231017@anycapital.com',226,2,'2017-10-23 13:01:03.144',NULL,NULL,0,'0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36','1','1',1,1,NULL,5,'test',NULL),(36,'test','test',2,'12345678','ea735f@anycapital.com',2,2,'2017-10-30 09:24:54.555',NULL,NULL,0,'0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36','1','1',1,1,NULL,1,'test!!!!',NULL),(43,NULL,NULL,2,NULL,'01c741@anycapital.com',2,2,'2017-11-02 15:28:23.783',NULL,NULL,0,'127.0.0.1',NULL,'1','1',1,1,NULL,NULL,NULL,NULL),(49,'test','test',5,'4456668',NULL,226,2,'2017-11-08 16:33:35.294',NULL,-1,0,NULL,NULL,'1','1',1,2,NULL,NULL,NULL,NULL),(50,'test','test',5,'4456668',NULL,226,2,'2017-11-08 16:53:53.438',NULL,3,0,NULL,NULL,'1','1',1,2,NULL,NULL,NULL,NULL),(51,'test','test',5,'4456668',NULL,226,2,'2017-11-08 16:58:21.994',NULL,3,0,NULL,NULL,'1','1',1,2,NULL,NULL,NULL,NULL),(52,'test','test',5,'4456668',NULL,226,2,'2017-11-08 16:59:20.751',NULL,3,0,NULL,NULL,'1','1',1,2,NULL,NULL,NULL,NULL),(55,NULL,NULL,2,NULL,'eyal.o@invest.com',226,2,'2017-11-09 12:10:23.964',NULL,NULL,0,'104.155.159.71',NULL,'1','1',1,1,NULL,NULL,NULL,NULL),(58,NULL,NULL,2,NULL,'09112017A@invest.com',226,1,'2017-11-09 13:19:46.050',NULL,NULL,0,'104.155.159.71',NULL,'1','1',1,1,NULL,NULL,NULL,NULL),(61,NULL,NULL,2,NULL,'6fe205@anycapital.com',226,2,'2017-11-09 15:55:40.491',NULL,NULL,0,'127.0.0.1',NULL,'1','1',1,1,NULL,NULL,NULL,NULL),(94,'test','test',2,'1234567','test@anycapital.com',226,1,'2017-12-11 08:36:33.094',NULL,NULL,0,'199.203.251.26','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36','1','1',1,1,NULL,2,'testttttttttttttttttt',NULL);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:12
