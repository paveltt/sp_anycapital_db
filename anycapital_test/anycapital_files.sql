-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `file_type_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `time_created` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `action_source_id` tinyint(4) NOT NULL,
  `writer_id` smallint(4) DEFAULT NULL COMMENT 'File added by this writer',
  `is_primary` binary(1) DEFAULT '0' COMMENT '1 if this file is the most update file from this type to this user',
  `status_id` smallint(6) NOT NULL DEFAULT '2',
  `updated_by_writer_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_fk_idx` (`user_id`),
  KEY `file_type_fk_idx` (`file_type_id`),
  KEY `action_source_fk_idx` (`action_source_id`),
  KEY `writer_fk_idx` (`writer_id`),
  KEY `updated_by_writer_files_fk_idx` (`updated_by_writer_id`),
  CONSTRAINT `writer_files_fk` FOREIGN KEY (`writer_id`) REFERENCES `writers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `action_source_files_fk` FOREIGN KEY (`action_source_id`) REFERENCES `action_source` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `file_type_files_fk` FOREIGN KEY (`file_type_id`) REFERENCES `file_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_files_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (34,'1493792237899_-_DSC_0662-1024x684.jpg',4,10002,'2017-06-12 06:17:09.680',2,5,'1',1,NULL),(35,'1494152712648_-_download.jpg',1,10124,'2017-05-07 10:25:11.331',2,21,'1',2,NULL),(36,'1494152736978_-_download.jpg',2,10124,'2017-05-07 10:25:35.659',2,21,'0',1,NULL),(38,'1495548366419_-_18930_en_1.jpg',2,10389,'2017-05-23 14:06:06.475',2,22,'1',1,NULL),(39,'1495548380008_-_19460_en_1.jpg',1,10389,'2017-05-23 14:06:20.064',2,22,'1',1,NULL),(40,'1495551708574_-_^7CC36E5AD14995174DF721A12B2074AE24385BBE153460A2AC^pimgpsh_thumbnail_win_distr.jpg',1,10391,'2017-05-23 15:01:48.576',2,3,'0',1,NULL),(41,'1497858786757_-_3d_black_ball-1680x1050.jpg',1,10325,'2017-06-19 07:53:06.763',2,22,'1',1,NULL),(42,'1497858833766_-_19460_en_1.jpg',2,10325,'2017-06-19 07:53:53.773',2,22,'1',1,NULL),(43,'1498742426805_-_dev-1342-a.PNG',2,10362,'2017-06-29 13:20:26.817',2,5,'0',1,NULL),(50,'1499678970077_-_screenshot-7.png',2,10177,'2017-07-10 09:29:30.089',2,3,'1',1,NULL),(51,'1499678973634_-_screenshot-7.png',2,10177,'2017-07-10 09:29:33.636',2,3,'0',1,NULL),(52,'1500972760966_-_MP.PNG',2,10600,'2017-07-25 08:52:40.972',2,3,'1',1,NULL),(53,'1500972898717_-_download.jpg',2,10600,'2017-07-25 08:54:58.729',2,3,'0',1,NULL),(54,'1501496000450_-_roundflower - Copy.jpg',1,10610,'2017-07-31 10:13:20.473',2,3,'0',1,NULL),(55,'1504254897535_-_Marcia4.jpg',3,10002,'2017-09-01 08:34:57.536',2,108,'0',1,NULL),(56,'1504254899190_-_Marcia4.jpg',3,10002,'2017-09-01 08:34:59.191',2,108,'1',1,NULL),(57,'1504767378892_-_mz.jpg',1,10124,'2017-09-07 06:56:18.892',2,3,'1',1,NULL),(58,'1504767439132_-_mz.jpg',2,10687,'2017-09-07 06:57:19.131',2,3,'1',1,NULL),(59,'1504770196759_-_mz.jpg',1,10686,'2017-09-07 07:43:16.757',2,3,'1',1,NULL),(62,'1506320554859_-_1.PNG',1,10002,'2017-09-25 06:22:34.951',2,-1,'0',1,NULL),(67,'1506323891700_-_1.PNG',1,10658,'2017-09-25 07:18:11.790',2,NULL,'1',1,NULL),(69,'1508054682969_-_download.jpg',2,10690,'2017-10-15 08:04:43.088',2,3,'0',2,NULL),(70,'1508140069062_-_download.jpg',3,10177,'2017-10-16 07:47:49.165',2,3,'1',2,NULL),(71,'1508140296824_-_download.jpg',4,10177,'2017-10-16 07:51:36.927',2,3,'1',2,NULL);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:50:14
