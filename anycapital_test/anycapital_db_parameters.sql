-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `db_parameters`
--

DROP TABLE IF EXISTS `db_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_parameters` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(30) NOT NULL,
  `NUM_VALUE` double DEFAULT NULL,
  `STRING_VALUE` varchar(100) DEFAULT NULL,
  `DATE_VALUE` datetime DEFAULT NULL,
  `COMMENTS` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_parameters`
--

LOCK TABLES `db_parameters` WRITE;
/*!40000 ALTER TABLE `db_parameters` DISABLE KEYS */;
INSERT INTO `db_parameters` VALUES (1,'max_inv',99999900,NULL,NULL,'Max investment'),(2,'minimum_withdraw',10000,NULL,NULL,'minimum withdraw amount'),(3,'withdraw_fee',3000,NULL,NULL,'withdraw fee amount'),(4,'user_tokens_time_expired',24,NULL,NULL,'time to expired user token. sysdate - this / 24'),(5,'google_short_URL_API',NULL,'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyCEeDx224YQbCKMo9VFkiU5vC89DNO3RN0',NULL,'google short URL API '),(6,'min_inv',300000,NULL,NULL,'Min investment'),(7,'sendgrid_api_key',NULL,'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',NULL,'Key for sendgrid api'),(8,'sendgrid_service_url',NULL,'https://api.sendgrid.com/v3/',NULL,'service url of sendgrid'),(9,'key_secure_aes',NULL,'TheBestKeyEver#1',NULL,'Key for encrypt and decrypt by AES algorithm'),(14,'email_sender_consumers',5,NULL,NULL,'number of consumers in the email sender job'),(15,'email_sender_queue_size',500,NULL,NULL,'queue size for email sender job'),(16,'support_phone',NULL,'+44-2033185586',NULL,'support phone to call'),(17,'support_email',NULL,'support@anycapital.com',NULL,'support email'),(18,'Company_Address',NULL,'company-address',NULL,'Company Address'),(19,'HC TTL analytics',300,NULL,NULL,'Time to left analytics info in hazelcast'),(20,'documents_email',NULL,'documents-test@anycapital.com',NULL,'Documents email');
/*!40000 ALTER TABLE `db_parameters` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:38
