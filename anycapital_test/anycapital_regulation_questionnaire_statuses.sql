-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `regulation_questionnaire_statuses`
--

DROP TABLE IF EXISTS `regulation_questionnaire_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulation_questionnaire_statuses` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'The questionnaire''s status id',
  `name` varchar(45) NOT NULL COMMENT 'The questionnaire''s status name',
  `display_name` varchar(45) NOT NULL COMMENT 'The questionnaire''s status key',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Represent the regulation status of the questionnaire (for example if user passed the questionnaire according to regulation process)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regulation_questionnaire_statuses`
--

LOCK TABLES `regulation_questionnaire_statuses` WRITE;
/*!40000 ALTER TABLE `regulation_questionnaire_statuses` DISABLE KEYS */;
INSERT INTO `regulation_questionnaire_statuses` VALUES (1,'Unfitting','regulation.questionnaire.status.1'),(2,'Fitting','regulation.questionnaire.status.2'),(3,'Fitting after training','regulation.questionnaire.status.3'),(4,'Unfitting Restricted','regulation.questionnaire.status.4');
/*!40000 ALTER TABLE `regulation_questionnaire_statuses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:06
