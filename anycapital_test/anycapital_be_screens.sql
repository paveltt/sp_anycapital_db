-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `be_screens`
--

DROP TABLE IF EXISTS `be_screens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `be_screens` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  `SPACE_ID` smallint(6) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `SPACE_ID` (`SPACE_ID`),
  CONSTRAINT `be_screens_ibfk_1` FOREIGN KEY (`SPACE_ID`) REFERENCES `be_spaces` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_screens`
--

LOCK TABLES `be_screens` WRITE;
/*!40000 ALTER TABLE `be_screens` DISABLE KEYS */;
INSERT INTO `be_screens` VALUES (1,'products',3),(2,'product-priorty',3),(3,'translations',2),(4,'investments',5),(5,'transactions',5),(6,'pending-withdraws-fa',5),(7,'pending-withdraws-sa',5),(8,'marketing-sources',4),(9,'marketing-lps',4),(10,'marketing-contents',4),(11,'marketing-campaigns',4),(12,'users',1),(13,'user-issues',1),(14,'user-questionnaire',1),(15,'files',1),(16,'regulation',1),(17,'user-change-pass',1),(18,'user-transactions',1),(19,'user-investments',1),(20,'user-wire-deposit',1),(21,'user-admin-deposit',1),(22,'user-wire-withdraw',1),(23,'user-admin-withdraw',1),(24,'client-space',1),(25,'db-parameters',5),(26,'writers',5),(27,'writer-change-pass',5),(28,'templates',1),(29,'user-advanced-search',1),(30,'admin-panel',5),(31,'bulk-translations',2),(32,'upload-contacts',4);
/*!40000 ALTER TABLE `be_screens` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:50:18
