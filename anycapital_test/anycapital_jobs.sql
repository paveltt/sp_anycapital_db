-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `RUNNING` binary(1) DEFAULT '0' COMMENT 'if the job is currently running',
  `LAST_RUN_TIME` datetime DEFAULT NULL COMMENT 'time the last run started',
  `CRON_EXPRESSION` varchar(100) DEFAULT NULL COMMENT 'cron expression execution time',
  `DESCRIPTION` varchar(256) DEFAULT NULL,
  `JOB_CLASS` varchar(100) DEFAULT NULL,
  `CONFIG` varchar(2000) DEFAULT NULL COMMENT 'Comma separated list of key=value pairs. To set job specific configs.',
  `IS_ACTIVE` binary(1) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (1,'2017-02-11 20:24:13.000','0','2018-02-05 00:00:05','05 0 0 1/1 * *','Products settle job','capital.any.job.ProductsSettlesJob',NULL,'1'),(2,'2017-02-11 20:24:13.000','0','2018-02-05 15:40:02','0 0,20,40 * * * *','Market job','capital.any.job.MarketJob',NULL,'1'),(3,'2017-02-11 20:24:13.000','0','2018-02-05 00:00:05','05 0 0 1/1 * *','Products Statuses Job','capital.any.job.ProductsStatusesJob',NULL,'1'),(4,'2017-02-11 20:24:13.000','0','2018-02-05 15:00:01','0 0 * * * *','Sendgrid Job','capital.any.job.SendgridJob',NULL,'1'),(5,'2017-02-11 20:24:13.000','0','2018-02-05 00:00:05','05 0 0 1/1 * *','Products Coupon job','capital.any.job.PayProductCouponsJob',NULL,'1'),(6,'2017-03-04 13:08:44.864','0','2018-02-05 15:49:20','0/10 * * * * *','SMS job','capital.any.job.SMSJob',NULL,'1'),(7,'2017-03-14 07:01:15.081','0','2018-02-05 01:00:06','0 0 1 1/1 * *','Markets history daily Job','capital.any.job.MarketDailyHistoryJob',NULL,'1'),(8,'2017-05-14 08:37:40.329','0','2017-12-29 06:36:20','* * * * * *','send emails job','capital.any.job.EmailSenderJob',NULL,'1'),(10,'2017-06-13 11:46:32.264','0','2018-02-05 02:00:00','0 0 2 1/1 * *','Daily report Job','capital.any.job.DailyReportJob',NULL,'1');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:27
