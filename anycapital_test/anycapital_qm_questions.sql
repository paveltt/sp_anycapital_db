-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `qm_questions`
--

DROP TABLE IF EXISTS `qm_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qm_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The question''s id.',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The question''s key will be replaced by text''s languages.',
  `is_active` binary(1) NOT NULL DEFAULT '1' COMMENT 'Represent if the record is active or not.',
  `order_id` int(11) NOT NULL COMMENT 'The orders of the questions.',
  `question_type_id` smallint(6) NOT NULL COMMENT 'Represent the question''s type.',
  `question_group_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_idx` (`question_type_id`),
  KEY `fk_question_group_idx` (`question_group_id`),
  CONSTRAINT `` FOREIGN KEY (`question_type_id`) REFERENCES `qm_question_types` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qm_questions`
--

LOCK TABLES `qm_questions` WRITE;
/*!40000 ALTER TABLE `qm_questions` DISABLE KEYS */;
INSERT INTO `qm_questions` VALUES (1,'What is your annual income?','q.1','1',1,1,1),(2,'What is your main source of income?','q.2','1',2,1,1),(3,'How much do you think you will trade each year?','q.3','0',3,1,1),(4,'What is the anticipated frequency of your transactions?','q.4','0',4,1,1),(5,'What is your level of education?','q.5','1',5,1,1),(6,'What is your current employment status?','q.6','1',6,1,1),(7,'What is your profession?','q.7','0',7,4,1),(8,'The extent of your trading experience can be estimated as:','q.8','0',8,1,1),(9,'Have you ever traded financial derivatives such as futures, options or commodities?','q.9','0',9,1,1),(10,'How many trades including derivatives, options, futures and commodities did you trade in the last 12 (twelve) months?','q.10','0',10,1,1),(11,'What was the average monthly volume of your past transactions in the above products?','q.11','0',11,1,1),(12,'What is your nature of trading:','q.12','1',26,1,1),(13,'I am the individual that is the beneficial owner of all the income in this account','q.13','0',13,2,1),(14,'I am at least 18 years of age','q.14','1',31,2,1),(15,'I am not a U.S. citizen or a U.S. resident, including a resident alien individual or other U.S. person (IRS Definitions)','q.15','1',32,2,1),(16,'I am not a politician or a holder of a politically exposed position (for example: Minister, Ambassador, Presiding Judge)','q.16','0',16,2,1),(17,'I understand the risks of binary options trading, as stated in the Risk Disclosure Notice','q.17','0',17,2,1),(18,'What is your net worth?','q.18','1',3,1,1),(19,'How much do you think you will invest each year?','q.19','1',4,1,1),(20,'What is your profession:','q.20','1',7,4,1),(21,'Binary - Experience','q.experience','1',8,4,2),(22,'Binary - Frequency','q.frequency','1',9,4,2),(23,'Binary - Annual volume','q.annual.volume','1',10,4,2),(24,'Futures - Experience','q.experience','1',11,4,3),(25,'Futures - Frequency','q.frequency','1',12,4,3),(26,'Futures - Annual volume','q.annual.volume','1',13,4,3),(27,'CFD’s / Forex - Experience','q.experience','1',14,4,4),(28,'CFD’s / Forex - Frequency','q.frequency','1',15,4,4),(29,'CFD’s / Forex - Annual volume','q.annual.volume','1',16,4,4),(30,'Cash deposits - Experience','q.experience','1',17,4,5),(31,'Cash deposits - Frequency','q.frequency','1',18,4,5),(32,'Cash deposits - Annual volume','q.annual.volume','1',19,4,5),(33,'Structured Products - Experience','q.experience','1',20,4,6),(34,'Structured Products - Frequency','q.frequency','1',21,4,6),(35,'Structured Products - Annual volume','q.annual.volume','1',22,4,6),(36,'Commodities - Experience','q.experience','1',23,4,7),(37,'Commodities - Frequency','q.frequency','1',24,4,7),(38,'Commodities - Annual volume','q.annual.volume','1',25,4,7),(39,'structured product - 1','q.39','1',27,4,1),(40,'structured product - 2','q.40','1',28,4,1),(41,'structured product - 3','q.41','1',29,4,1),(42,'I confirm that I am registering this account in my own name','q.42','1',30,2,1),(43,'I am not a “Politically Exposed Person”.','q.43','1',33,2,1),(44,'I understand the risks of investing in Structured Products','q.44','1',34,2,1);
/*!40000 ALTER TABLE `qm_questions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:55
