-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `writer_group_permissions`
--

DROP TABLE IF EXISTS `writer_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `writer_group_permissions` (
  `GROUP_ID` smallint(6) NOT NULL,
  `BE_SCREEN_ID` smallint(6) NOT NULL,
  `BE_PERMISSION_ID` smallint(6) NOT NULL,
  `IS_ACTIVE` binary(1) DEFAULT '1',
  KEY `GROUP_ID` (`GROUP_ID`),
  KEY `BE_SCREEN_ID` (`BE_SCREEN_ID`),
  KEY `BE_PERMISSION_ID` (`BE_PERMISSION_ID`),
  CONSTRAINT `writer_group_permissions_ibfk_1` FOREIGN KEY (`GROUP_ID`) REFERENCES `writer_groups` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `writer_group_permissions_ibfk_2` FOREIGN KEY (`BE_SCREEN_ID`) REFERENCES `be_screens` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `writer_group_permissions_ibfk_3` FOREIGN KEY (`BE_PERMISSION_ID`) REFERENCES `be_permissions` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `writer_group_permissions`
--

LOCK TABLES `writer_group_permissions` WRITE;
/*!40000 ALTER TABLE `writer_group_permissions` DISABLE KEYS */;
INSERT INTO `writer_group_permissions` VALUES (1,2,1,'1'),(1,3,1,'1'),(1,3,1,'1'),(1,3,1,'1'),(1,4,1,'1'),(1,5,1,'1'),(1,6,1,'1'),(1,7,1,'1'),(1,8,1,'1'),(1,8,1,'1'),(1,8,1,'1'),(1,9,1,'1'),(1,9,1,'1'),(1,9,1,'1'),(1,10,1,'1'),(1,10,1,'1'),(1,10,1,'1'),(1,11,1,'1'),(1,11,1,'1'),(1,11,1,'1'),(1,12,1,'1'),(1,12,1,'1'),(1,12,1,'1'),(1,13,1,'1'),(1,13,1,'1'),(1,14,1,'1'),(1,15,1,'1'),(1,15,1,'1'),(1,15,1,'1'),(1,16,1,'1'),(1,17,1,'1'),(1,18,1,'1'),(1,19,1,'1'),(1,20,1,'1'),(1,21,1,'1'),(1,22,1,'1'),(1,23,1,'1'),(1,24,1,'1'),(3,1,1,'1'),(3,1,2,'1'),(3,1,3,'1'),(3,2,1,'1'),(3,4,1,'1'),(3,12,1,'1'),(3,12,2,'1'),(3,12,3,'1'),(3,13,1,'1'),(3,13,3,'1'),(3,14,1,'1'),(3,18,1,'1'),(3,19,1,'1'),(3,24,1,'1'),(4,3,1,'1'),(4,3,2,'1'),(4,3,3,'1'),(5,8,1,'1'),(5,8,2,'1'),(5,8,3,'1'),(5,9,1,'1'),(5,9,2,'1'),(5,9,3,'1'),(5,10,1,'1'),(5,10,2,'1'),(5,10,3,'1'),(5,11,1,'1'),(5,11,2,'1'),(5,11,3,'1'),(5,18,1,'1'),(5,19,1,'1'),(5,24,1,'1'),(5,12,1,'1'),(1,4,1,'1'),(1,5,1,'1'),(1,6,1,'1'),(1,7,1,'1'),(1,12,1,'1'),(1,12,2,'1'),(1,12,3,'1'),(1,13,1,'1'),(1,13,3,'1'),(1,14,1,'1'),(1,15,1,'1'),(1,15,2,'1'),(1,15,3,'1'),(1,18,1,'1'),(1,19,1,'1'),(1,20,1,'1'),(1,21,1,'1'),(1,22,1,'1'),(1,23,1,'1'),(1,24,1,'1'),(1,25,1,'1'),(1,25,1,'1'),(1,25,1,'1'),(1,26,1,'1'),(1,26,1,'1'),(1,26,1,'1'),(1,27,1,'1'),(1,27,1,'1'),(3,27,1,'1'),(4,27,1,'1'),(5,27,1,'1'),(1,28,1,'1'),(2,28,1,'1'),(2,4,1,'1'),(2,5,1,'1'),(2,6,1,'1'),(2,12,1,'1'),(2,12,2,'1'),(2,12,3,'1'),(2,13,1,'1'),(2,13,3,'1'),(2,14,1,'1'),(2,15,1,'1'),(2,15,2,'1'),(2,15,3,'1'),(2,18,1,'1'),(2,19,1,'1'),(2,20,1,'1'),(2,21,1,'1'),(2,22,1,'1'),(2,23,1,'1'),(2,24,1,'1'),(2,27,1,'1'),(6,4,1,'1'),(6,5,1,'1'),(6,6,1,'1'),(6,7,1,'1'),(6,12,1,'1'),(6,12,2,'1'),(6,12,3,'1'),(6,13,1,'1'),(6,13,3,'1'),(6,14,1,'1'),(6,15,1,'1'),(6,15,2,'1'),(6,15,3,'1'),(6,16,1,'1'),(6,18,1,'1'),(6,19,1,'1'),(6,20,1,'1'),(6,21,1,'1'),(6,22,1,'1'),(6,23,1,'1'),(6,24,1,'1'),(6,27,1,'1'),(6,28,1,'1'),(1,1,3,'1'),(1,1,2,'1'),(1,1,1,'1'),(1,29,1,'1'),(1,29,1,'1'),(1,30,1,'1'),(1,31,1,'1'),(1,32,1,'1');
/*!40000 ALTER TABLE `writer_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:48:08
