-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `writers`
--

DROP TABLE IF EXISTS `writers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `writers` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `time_created` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `USER_NAME` varchar(40) NOT NULL,
  `PASSWORD` varchar(15) NOT NULL,
  `FIRST_NAME` varchar(20) CHARACTER SET utf8 NOT NULL,
  `LAST_NAME` varchar(20) CHARACTER SET utf8 NOT NULL,
  `EMAIL` varchar(50) CHARACTER SET utf8 NOT NULL,
  `COMMENTS` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `IS_ACTIVE` binary(1) DEFAULT '1',
  `UTC_OFFSET` smallint(6) NOT NULL DEFAULT '-180',
  `GROUP_ID` smallint(6) DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`),
  UNIQUE KEY `USER_NAME` (`USER_NAME`),
  KEY `GROUP_ID` (`GROUP_ID`),
  CONSTRAINT `writers_ibfk_1` FOREIGN KEY (`GROUP_ID`) REFERENCES `writer_groups` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `writers`
--

LOCK TABLES `writers` WRITE;
/*!40000 ALTER TABLE `writers` DISABLE KEYS */;
INSERT INTO `writers` VALUES (-1,'2017-01-01 10:33:54.000','junit','123456','j','unit','junit@anyoption.com',NULL,'1',-180,2),(2,'2016-08-11 09:55:47.000','miki','123456','Miki','F','miki@anyoption.com',NULL,'0',-180,1),(3,'2016-07-22 08:41:41.000','eran','123123','Eran','Levy','eran.levy@anycapital.com',NULL,'1',-180,1),(4,'2016-08-01 09:53:06.000','lior','1','LioR','SoLoMoN','liors@etrader.co.il',NULL,'0',-180,1),(5,'2016-08-01 09:53:06.000','eyalg','123456','Eyal','Goren','eyal.goren@anyoption.com',NULL,'0',-180,1),(6,'2016-08-01 09:53:06.000','eyalo','123456','Eyal','Ohana','eyal.ohana@anyoption.com',NULL,'1',-180,1),(8,'2016-08-11 09:55:47.000','petar','123456','Petar','Tchorbadjiyski','petart@anyoption.com',NULL,'1',-180,1),(9,'2016-08-11 09:55:47.000','support','123456','support','support','support@any.capital',NULL,'0',-180,2),(21,'2016-10-26 10:09:47.000','mira','123456','mira','Kofman','mira.kofman@anyoption.com',NULL,'0',-180,1),(22,'2017-03-19 12:29:21.000','kosta','123456','Kosta','Romanov','kosta.romanov@anyoption.com',NULL,'0',-180,1),(41,'2017-01-02 06:27:47.000','alexander','123456','alexander','Z','alexander.z@anyoption.com',NULL,'0',-180,1),(50,'2017-06-16 07:22:15.532','maia','123456','Maia','Naor','maia.naor@anycapital.com',NULL,'0',-180,1),(100,'2017-07-27 13:22:24.709','content','123456','content','content','content@anycapital.com',NULL,'0',-180,4),(101,'2017-07-27 13:23:12.026','admin','123456','admin','admin','admin@anycapital.com',NULL,'0',-180,1),(102,'2017-07-27 13:23:12.035','trader','123456','trader','trader','trader@anycapital.com',NULL,'0',-180,3),(103,'2017-07-27 13:23:12.035','marketing','123456','marketing','marketing','marketing@anycapital.com',NULL,'0',-180,5),(104,'2017-07-27 13:23:12.035','kemal','123456','Kemal','Levent','kemal.l@invest.com',NULL,'0',-180,5),(105,'2017-08-06 13:23:12.035','shlomit','654321','Shlomit','Maharat','shlomitm@anyoption.com',NULL,'0',-180,2),(106,'2017-08-06 13:23:12.035','roman','654321','Roman','Lustenberger','roman.l@invest.com',NULL,'0',-180,2),(107,'2017-08-06 13:23:12.035','varda','654321','Varda','Bachrach','varda.b@invest.com',NULL,'0',-180,4),(108,'2017-08-23 11:12:48.521','michael.p','MP123456!','Michael','Paul','michael.paul@anyoption.com',NULL,'0',-180,6),(109,'2017-08-23 11:14:06.896','anna.m','AM123456!','Anna','Mavridou','anna.mavridou@ouroboros.com.cy',NULL,'0',-180,6),(110,'2017-10-30 12:22:03.745','maria.l','123456','Mariya','Lazarova','mariya.l@invest.com',NULL,'1',-180,1),(120,'2017-12-05 08:05:25.996','pavel.t','Pavel123456!ts','Pavel','Tabakov','pavel.t@invest.com',NULL,'1',-180,1),(122,'2017-12-14 08:19:05.422','georgi.di','Aa123456','Georgi','Diamandiev','georgi.di@invest.com',NULL,'1',-180,1),(123,'2017-12-14 08:20:31.060','pavlin.m','Aa123456','Pavlin','Mihalev','pavlin.m@invest.com',NULL,'1',-180,1),(124,'2017-12-14 08:23:00.690','kiril.m','Aa123456','Kiril','Mutafchiev','kiril.m@invest.com',NULL,'1',-180,1);
/*!40000 ALTER TABLE `writers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:50:45
