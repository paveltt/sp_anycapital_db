-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `investments`
--

DROP TABLE IF EXISTS `investments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investments` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL,
  `INVESTMENT_STATUS_ID` smallint(6) NOT NULL,
  `AMOUNT` bigint(20) DEFAULT NULL COMMENT 'amonut in EUR',
  `RETURN_AMOUNT` bigint(20) DEFAULT NULL COMMENT 'the amount that will retun to the user balance after settlement',
  `INVESTMENT_TYPE_ID` smallint(6) NOT NULL,
  `ASK` double DEFAULT NULL,
  `BID` double DEFAULT NULL,
  `RATE` float NOT NULL COMMENT 'rate to convert amount to usd',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `ORIGINAL_AMOUNT` bigint(20) NOT NULL COMMENT 'The amount in product currency',
  `ORIGINAL_RETURN_AMOUNT` bigint(20) DEFAULT NULL COMMENT 'The return amount in product currency',
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`),
  KEY `INVESTMENT_STATUS_ID` (`INVESTMENT_STATUS_ID`),
  KEY `INVESTMENT_TYPE_ID` (`INVESTMENT_TYPE_ID`),
  CONSTRAINT `investments_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_ibfk_2` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_ibfk_3` FOREIGN KEY (`INVESTMENT_STATUS_ID`) REFERENCES `investment_statuses` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_ibfk_4` FOREIGN KEY (`INVESTMENT_TYPE_ID`) REFERENCES `investment_types` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10261 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investments`
--

LOCK TABLES `investments` WRITE;
/*!40000 ALTER TABLE `investments` DISABLE KEYS */;
INSERT INTO `investments` VALUES (10092,10362,10160,2,891107,NULL,1,0,0,1,'2017-06-12 15:21:37.734',1000000,NULL),(10093,10325,10135,2,446150,NULL,1,0,0,1,'2017-06-13 11:16:34.847',500000,NULL),(10094,10325,10135,2,353911,NULL,1,0,0,1,'2017-06-14 08:29:21.119',396734,NULL),(10095,10325,10135,1,892061,NULL,1,0,0,1,'2017-06-14 08:29:33.334',1000000,NULL),(10096,10535,10136,2,300000,NULL,1,0,0,1,'2017-06-15 13:12:23.985',300000,NULL),(10097,10168,10136,2,1000000,NULL,1,0,0,1,'2017-06-15 13:36:32.589',1000000,NULL),(10098,10168,10136,2,300000,NULL,1,0,0,1,'2017-06-15 13:38:29.010',300000,NULL),(10099,10168,10136,2,300000,NULL,1,0,0,1,'2017-06-15 13:38:35.906',300000,NULL),(10100,10168,10136,2,300000,NULL,1,0,0,1,'2017-06-15 13:38:41.695',300000,NULL),(10101,10168,10150,2,446668,NULL,1,0,0,1,'2017-06-15 14:16:26.159',500000,NULL),(10102,10168,10150,3,446668,10000,1,0,0,1,'2017-06-15 14:16:38.416',500000,10000),(10103,10168,10178,3,300000,303030,2,99,0,1,'2017-06-15 15:24:40.232',300000,303030),(10104,10168,10178,3,300000,303030,2,99,0,1,'2017-06-16 05:29:54.888',300000,303030),(10105,10168,10178,3,300000,303030,2,99,0,1,'2017-06-16 05:30:00.383',300000,303030),(10106,10168,10178,3,300000,303030,2,99,0,1,'2017-06-16 05:30:05.274',300000,303030),(10107,10168,10178,3,300000,303030,2,99,0,1,'2017-06-16 05:30:10.041',300000,303030),(10108,10168,10178,3,300000,303030,2,99,0,1,'2017-06-16 05:30:14.786',300000,303030),(10109,10168,10178,3,300000,303030,2,99,0,1,'2017-06-16 05:30:21.545',300000,303030),(10110,10168,10178,2,300000,NULL,2,99,0,1,'2017-06-16 05:32:59.858',300000,NULL),(10111,10168,10178,2,300000,NULL,2,99,0,1,'2017-06-16 05:33:05.802',300000,NULL),(10112,10168,10178,3,300000,303030,2,99,0,1,'2017-06-16 05:33:11.346',300000,303030),(10113,10177,10136,2,980000,NULL,1,0,0,1,'2017-06-26 09:53:31.850',980000,NULL),(10114,10177,10136,2,900000,NULL,1,0,0,1,'2017-06-26 10:02:38.094',900000,NULL),(10115,10577,10135,3,878812,703977,1,0,0,1,'2017-07-03 09:13:30.350',1000000,800000),(10116,10577,10135,3,439898,293266,2,120,0,1,'2017-07-03 12:15:43.026',499900,333267),(10117,10577,10135,3,439898,293266,2,120,0,1,'2017-07-03 12:18:12.324',499900,333267),(10118,10577,10135,3,879972,586648,2,120,0,1,'2017-07-03 12:46:25.343',1000000,666667),(10119,10577,10135,3,439898,293266,2,120,0,1,'2017-07-03 12:53:35.822',499900,333267),(10120,10577,10135,2,879972,NULL,2,120,0,1,'2017-07-03 13:23:26.851',1000000,NULL),(10121,10577,10135,3,439898,293266,2,120,0,1,'2017-07-03 13:28:32.516',499900,333267),(10122,10577,10136,3,300000,303000,1,0,0,1,'2017-07-04 06:31:42.368',300000,303000),(10123,10177,10135,3,979633,653492,2,120,0,1,'2017-07-05 09:18:41.667',1111100,740733),(10124,10177,10135,2,882223,NULL,2,120,0,1,'2017-07-05 09:23:51.186',1000000,NULL),(10125,10177,10135,2,1146890,NULL,2,120,0,1,'2017-07-05 09:24:21.648',1300000,NULL),(10126,10177,10135,2,441345,NULL,2,120,0,1,'2017-07-05 09:40:14.485',500000,NULL),(10127,10177,10168,2,1000000,NULL,1,0,0,1,'2017-07-05 13:21:34.796',1000000,NULL),(10128,10579,10168,5,1000000,NULL,1,0,0,1,'2017-07-05 13:23:47.657',1000000,NULL),(10129,10579,10136,5,1300000,NULL,1,0,0,1,'2017-07-05 13:39:27.997',1300000,NULL),(10130,10105,10135,2,438673,NULL,2,120,0,1,'2017-07-09 07:11:25.275',500000,NULL),(10131,10593,10162,2,1000000,NULL,1,0,0,1,'2017-07-09 09:05:19.448',1000000,NULL),(10132,10593,10135,2,877347,NULL,2,120,0,1,'2017-07-09 09:08:05.903',1000000,NULL),(10133,10593,10150,5,877347,NULL,1,0,0,1,'2017-07-09 09:13:59.289',1000000,NULL),(10134,10592,10135,2,876655,NULL,2,120,0,1,'2017-07-10 07:42:23.372',1000000,NULL),(10135,10592,10162,2,1000000,NULL,1,0,0,1,'2017-07-10 07:43:50.007',1000000,NULL),(10136,10592,10163,2,1000000,NULL,1,0,0,1,'2017-07-10 08:03:29.606',1000000,NULL),(10137,10592,10135,2,876655,NULL,2,120,0,1,'2017-07-10 08:13:58.974',1000000,NULL),(10138,10592,10163,2,1000000,NULL,1,0,0,1,'2017-07-10 08:17:56.312',1000000,NULL),(10139,10124,10135,2,438289,NULL,2,120,0,1,'2017-07-11 14:19:16.534',500000,NULL),(10140,10124,10135,2,438558,NULL,2,120,0,1,'2017-07-11 14:29:56.162',500000,NULL),(10141,10124,10135,2,436376,NULL,2,120,0,1,'2017-07-12 07:43:32.107',500000,NULL),(10142,10593,10179,5,873134,NULL,1,0,0,1,'2017-07-12 11:03:14.720',1000000,NULL),(10143,10168,10179,2,437331,NULL,1,0,0,1,'2017-07-13 07:44:54.465',500000,NULL),(10144,10177,10150,2,476456,NULL,2,103,0,1,'2017-07-23 09:38:25.272',555500,NULL),(10145,10600,10135,2,429037,NULL,2,102,0,1,'2017-07-25 09:10:23.349',500000,NULL),(10148,10105,10182,2,300000,NULL,1,0,0,1,'2017-07-30 12:19:33.725',300000,NULL),(10149,10105,10182,2,300000,NULL,1,0,0,1,'2017-07-30 12:32:26.951',300000,NULL),(10150,10105,10182,2,300000,NULL,1,0,0,1,'2017-07-30 12:36:54.855',300000,NULL),(10151,10105,10182,2,900000,NULL,1,0,0,1,'2017-07-30 12:52:33.071',900000,NULL),(10152,10103,10182,5,900000,NULL,1,0,0,1,'2017-07-30 13:22:03.598',900000,NULL),(10153,10105,10182,2,7761300,NULL,1,0,0,1,'2017-07-30 13:26:19.848',7761300,NULL),(10155,10611,10183,2,555500,NULL,1,0,0,1,'2017-07-31 10:26:12.297',555500,NULL),(10159,10577,10136,2,1000000,NULL,2,103,0,1,'2017-07-31 15:08:35.719',1000000,NULL),(10160,10577,10136,3,1000000,980583,2,103,0,1,'2017-07-31 15:10:29.099',1000000,980583),(10161,10577,10136,2,5555000,NULL,2,103,0,1,'2017-07-31 15:10:54.872',5555000,NULL),(10162,10656,10135,2,425170,NULL,2,102,0,1,'2017-08-20 11:19:11.616',500000,NULL),(10163,10656,10135,2,472364,NULL,2,102,0,1,'2017-08-20 11:19:27.404',555500,NULL),(10214,10657,10188,1,500000,NULL,1,0,0,1,'2017-08-20 13:19:29.763',500000,NULL),(10226,10124,10136,3,777700,762599,2,103,0,1,'2017-08-22 09:27:55.242',777700,762599),(10260,10124,10135,2,843400,NULL,2,102,0,1,'2017-12-04 10:19:40.783',1000000,NULL);
/*!40000 ALTER TABLE `investments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:51:19
