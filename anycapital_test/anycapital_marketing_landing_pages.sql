-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marketing_landing_pages`
--

DROP TABLE IF EXISTS `marketing_landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketing_landing_pages` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Landing page''s id',
  `NAME` varchar(50) NOT NULL COMMENT 'Landing page''s name',
  `PATH` varchar(4000) NOT NULL COMMENT 'Landing page''s path',
  `WRITER_ID` smallint(6) NOT NULL COMMENT 'Foreign key to WRITERS',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'Landing page''s time created',
  `TIME_MODIFIED` timestamp NULL DEFAULT NULL COMMENT 'Landing page''s time modified',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME_UNIQUE` (`NAME`),
  KEY `WRITER_ID` (`WRITER_ID`),
  CONSTRAINT `marketing_landing_pages_ibfk_1` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_landing_pages`
--

LOCK TABLES `marketing_landing_pages` WRITE;
/*!40000 ALTER TABLE `marketing_landing_pages` DISABLE KEYS */;
INSERT INTO `marketing_landing_pages` VALUES (1,'home_page','',6,'2017-02-11 20:38:04.000','2017-03-13 14:14:43'),(2,'lp test update','terms',-1,'2017-02-12 14:03:23.000','2017-08-08 12:09:44'),(3,'terms_conditions','terms',6,'2017-02-22 12:39:34.449',NULL),(25,'register','register',3,'2017-08-08 12:09:44.748','2017-08-26 17:09:10');
/*!40000 ALTER TABLE `marketing_landing_pages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:48:34
