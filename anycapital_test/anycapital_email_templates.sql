-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_templates` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `action_id` smallint(6) NOT NULL COMMENT 'Foreign key to email_actions.',
  `language_id` smallint(6) NOT NULL COMMENT 'Foreign key to languages.',
  `template_id` varchar(100) NOT NULL COMMENT 'The template id which defined in Sendgrid system.',
  `is_active` binary(1) NOT NULL DEFAULT '1' COMMENT 'Represent if the record is active or not.',
  PRIMARY KEY (`id`),
  KEY `fk_et_action_id_idx` (`action_id`),
  KEY `fk_et_language_id_idx` (`language_id`),
  CONSTRAINT `fk_et_action_id` FOREIGN KEY (`action_id`) REFERENCES `email_actions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_et_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_templates`
--

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
INSERT INTO `email_templates` VALUES (5,1,2,'90633575-f226-4690-ac8f-3085bb8e5d88','1'),(6,2,2,'da9bf98e-7977-4e15-9d31-09a67ca681a1','1'),(7,8,2,'cc84f3dd-bc31-4fc0-b561-e61f2dda6eff','1'),(8,7,2,'cf51238c-0758-4ea8-bf8e-246003fc3979','1'),(9,9,2,'c5bb42f9-19ac-4967-86df-38d5a209e898','1'),(10,10,2,'de002efb-a9ae-4ce5-bd50-11e74e6d9719','1'),(11,11,2,'0cd29363-4f92-4f5b-a041-fb0895b81b6b','1'),(12,12,2,'96477449-eede-4919-83b9-2d7d99bc41c9','1'),(13,13,2,'78030a99-4bb2-4065-a974-1413a72d7e9d','1'),(14,1,1,'92bc2b5a-7041-46cb-97e6-0d6bd0f398cb','1'),(15,2,1,'524f0f67-eaf8-4668-b18b-5b8de8a3790c','1'),(16,8,1,'d57802f8-975c-444d-9627-32a2d75c9cf9','1'),(17,7,1,'52e323d1-a381-4b6c-bfca-7cc47b6393ec','1'),(18,9,1,'bd542918-f1eb-43c5-9207-9458bd957906','1'),(19,10,1,'b512cbe4-a4b5-4c48-bacb-e33b0b7ea3cf','1'),(20,11,1,'55698896-fe19-47d2-95fb-2decaae4fe60','1'),(21,12,1,'0394762d-78af-4db9-b931-a70c8dd5e03f','1'),(22,13,1,'68171892-ba4b-4bde-a94e-92f1cb9660f0','1'),(23,14,2,'80a4c864-98ab-482d-9204-9f29dccce148','1'),(24,14,1,'a31971d3-bbe7-41ca-8391-81a59c73b081','1');
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:07
