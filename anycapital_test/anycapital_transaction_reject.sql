-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_reject`
--

DROP TABLE IF EXISTS `transaction_reject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_reject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `transaction_payment_type_id` smallint(6) NOT NULL,
  `transaction_reject_type_id` smallint(6) NOT NULL,
  `transaction_operation_id` tinyint(4) NOT NULL,
  `Amount` int(11) NOT NULL,
  `action_source_id` tinyint(4) NOT NULL,
  `writer_id` smallint(6) DEFAULT NULL,
  `info` varchar(250) DEFAULT NULL,
  `SESSION_ID` varchar(50) DEFAULT NULL,
  `server_name` varchar(50) NOT NULL,
  `time_created` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  KEY `fk_transaction_payment_type_id_idx` (`transaction_payment_type_id`),
  KEY `fk_action_source_id_idx` (`action_source_id`),
  KEY `fk_writer_id_idx` (`writer_id`),
  KEY `fk_transaction_reject_type_id_idx` (`transaction_reject_type_id`),
  KEY `fk_transaction_operation_id_idx` (`transaction_operation_id`),
  CONSTRAINT `fk_action_source_id` FOREIGN KEY (`action_source_id`) REFERENCES `action_source` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_operation_id` FOREIGN KEY (`transaction_operation_id`) REFERENCES `transaction_operations` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_payment_type_id` FOREIGN KEY (`transaction_payment_type_id`) REFERENCES `transaction_payment_types` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_reject_type_id` FOREIGN KEY (`transaction_reject_type_id`) REFERENCES `transaction_reject_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_writer_id` FOREIGN KEY (`writer_id`) REFERENCES `writers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_reject`
--

LOCK TABLES `transaction_reject` WRITE;
/*!40000 ALTER TABLE `transaction_reject` DISABLE KEYS */;
INSERT INTO `transaction_reject` VALUES (2,1,1,4,2,200,1,-1,'info','session','server-name','2017-06-12 12:04:05.783'),(3,1,1,1,2,2000,2,4,'info','session','il-liors-pc\n','2017-06-13 11:29:59.955'),(4,10005,1,1,2,200,1,NULL,'info','session','il-liors-pc\n','2017-06-13 12:00:33.803'),(5,10535,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 13:17:39.248'),(6,10535,1,2,2,4000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 13:17:46.594'),(7,10535,1,2,2,9000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 13:17:51.982'),(8,10535,1,2,2,9900,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 13:17:56.660'),(9,10168,1,3,2,10000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 13:33:48.433'),(10,10168,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 13:44:22.865'),(11,10168,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 13:44:31.355'),(12,10535,1,3,2,10000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 14:04:56.551'),(13,10535,1,4,2,10000000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 14:05:58.550'),(14,10535,1,3,2,1000000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 14:06:04.412'),(15,10535,1,4,2,10000000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 14:06:13.480'),(16,10535,1,3,2,100000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-14 14:55:38.803'),(17,10535,1,3,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-15 07:22:10.650'),(18,10168,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-16 06:05:43.700'),(19,10168,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-16 06:05:48.518'),(20,10168,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-16 06:06:27.849'),(21,10168,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-16 06:10:04.673'),(22,10168,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-16 06:54:00.563'),(23,10177,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-16 06:55:25.378'),(24,10177,1,2,2,4000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-16 06:55:39.074'),(25,10177,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 05:51:56.454'),(26,10177,1,2,2,4000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 05:52:01.854'),(27,10177,1,4,2,960000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 05:52:12.793'),(28,10177,1,4,2,9999900,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 05:56:19.941'),(29,10177,1,3,2,30000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:19:24.239'),(30,10535,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:36:44.048'),(31,10535,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:36:50.147'),(32,10535,1,1,2,2900,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:36:55.959'),(33,10535,1,2,2,3100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:36:58.439'),(34,10535,1,2,2,6000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:37:01.751'),(35,10535,1,2,2,9000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:37:04.764'),(36,10535,1,2,2,9900,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:37:06.618'),(37,10535,1,3,2,10100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:37:37.798'),(38,10535,1,3,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:37:43.659'),(39,10535,1,3,2,200,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:37:45.566'),(40,10535,1,3,2,300,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:37:47.326'),(41,10535,1,3,2,300,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:38:47.134'),(42,10535,1,3,2,100000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:38:49.771'),(43,10535,1,4,2,1000000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:38:51.376'),(44,10535,1,3,2,90000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:39:00.901'),(45,10535,1,3,2,900000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:39:02.273'),(46,10535,1,4,2,9000000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-19 06:39:03.497'),(47,10168,1,3,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-20 12:19:54.630'),(48,10168,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-20 12:20:15.408'),(49,10168,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-20 12:20:22.443'),(50,10168,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-20 12:32:10.842'),(51,10168,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-06-20 12:32:28.552'),(52,1,3,1,2,200,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 10:45:42.175'),(53,10535,3,1,2,200,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 10:46:25.390'),(54,10535,3,1,2,2200,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 10:46:28.630'),(55,1,3,1,2,200,2,21,'info','session','ilww01anycapital.anycapital.com\n','2017-06-22 10:47:51.158'),(56,1,3,1,2,2200,2,21,'info','session','ilww01anycapital.anycapital.com\n','2017-06-22 10:47:54.146'),(57,1,1,2,2,4300,2,21,'info','session','ilww01anycapital.anycapital.com\n','2017-06-22 10:49:21.389'),(58,1,1,1,2,300,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 11:44:42.986'),(59,1,1,1,2,200,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 12:56:50.806'),(60,1,1,1,2,200,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 13:26:48.244'),(61,1,1,1,2,200,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 13:27:21.490'),(62,1,1,1,2,200,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 13:27:56.953'),(63,1,1,1,2,200,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-22 13:36:35.820'),(64,1,1,1,2,100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 06:23:19.664'),(65,1,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 06:53:16.664'),(66,10168,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 06:54:04.035'),(67,10168,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 06:54:16.312'),(68,10325,1,4,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 06:54:59.399'),(69,1,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 07:04:00.744'),(70,2,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 07:04:16.399'),(71,10002,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 07:05:38.406'),(72,10103,1,4,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 07:35:43.993'),(73,10116,1,4,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 07:36:06.177'),(74,10122,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 07:42:22.683'),(75,10155,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 08:24:44.103'),(76,10168,1,3,2,11100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 08:25:23.254'),(77,10184,1,1,2,100,2,21,'info','session','IL-MIRAKO-PC\n','2017-06-25 08:26:00.840'),(78,10577,1,1,2,200,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-04 13:51:47.298'),(79,10577,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-04 13:52:00.416'),(80,10577,1,1,2,300,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-04 13:52:10.011'),(81,10577,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-04 13:53:36.273'),(82,10577,1,1,2,300,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-04 13:53:52.991'),(83,10577,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-04 13:54:12.472'),(84,10577,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-04 13:55:12.739'),(85,10577,1,2,2,4000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-04 13:55:16.262'),(86,1,1,4,2,200,1,-1,'info','session','server-name','2017-07-07 10:08:39.117'),(87,1,1,4,2,200,1,-1,'info','session','server-name','2017-07-07 10:12:52.856'),(88,1,1,4,2,200,1,-1,'info','session','server-name','2017-07-07 10:58:07.995'),(89,1,1,4,2,200,1,-1,'info','session','server-name','2017-07-07 11:10:30.624'),(90,1,1,4,2,200,1,-1,'info','session','server-name','2017-07-07 11:20:16.351'),(91,1,1,4,2,200,1,-1,'info','session','server-name','2017-07-07 11:28:18.655'),(94,10168,1,3,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-18 10:02:28.597'),(95,10168,1,3,2,10000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-18 10:02:33.382'),(96,10577,1,2,2,3000,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-31 15:37:20.133'),(97,10577,1,1,2,100,1,NULL,'info','session','ilww01anycapital.anycapital.com\n','2017-07-31 15:37:32.394'),(106,1,1,4,2,200,1,-1,'info','session','server-name','2017-08-24 10:48:18.029'),(107,10002,1,3,2,10000,2,6,'info','session','wwtest00.anycapital.com\n','2017-08-29 12:56:16.036'),(108,10002,1,3,2,10000,2,6,'info','session','wwtest00.anycapital.com\n','2017-08-29 13:00:39.617'),(116,10002,1,3,2,25000,2,3,'info','session','wwtest00.anycapital.com\n','2017-11-29 10:04:02.089'),(117,10002,1,3,2,25000,2,3,'info','session','wwtest00.anycapital.com\n','2017-11-29 10:04:11.937');
/*!40000 ALTER TABLE `transaction_reject` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:50:15
