-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marketing_campaigns`
--

DROP TABLE IF EXISTS `marketing_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketing_campaigns` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Campaign''s id',
  `NAME` varchar(100) NOT NULL COMMENT 'Campaign''s name',
  `SOURCE_ID` smallint(6) DEFAULT NULL COMMENT 'Foreign key to MARKETING_SOURCES',
  `WRITER_ID` smallint(6) NOT NULL COMMENT 'Foreign key to WRITERS',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'Campaign''s time created',
  `TIME_MODIFIED` timestamp NULL DEFAULT NULL COMMENT 'Campaign''s time updated',
  `DOMAIN_ID` smallint(6) DEFAULT NULL COMMENT 'Foreign key to MARKETING_DOMAINS',
  `LANDING_PAGE_ID` smallint(6) DEFAULT NULL COMMENT 'Foreign key to MARKETING_LANDING_PAGES',
  `CONTENT_ID` smallint(6) DEFAULT NULL COMMENT 'Foreign key to MARKETING_CONTENTS',
  PRIMARY KEY (`ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  KEY `SOURCE_ID` (`SOURCE_ID`),
  KEY `DOMAIN_ID` (`DOMAIN_ID`),
  KEY `LANDING_PAGE_ID` (`LANDING_PAGE_ID`),
  KEY `CONTENT_ID` (`CONTENT_ID`),
  CONSTRAINT `marketing_campaigns_ibfk_1` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `marketing_campaigns_ibfk_2` FOREIGN KEY (`SOURCE_ID`) REFERENCES `marketing_sources` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `marketing_campaigns_ibfk_3` FOREIGN KEY (`DOMAIN_ID`) REFERENCES `marketing_domains` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `marketing_campaigns_ibfk_4` FOREIGN KEY (`LANDING_PAGE_ID`) REFERENCES `marketing_landing_pages` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `marketing_campaigns_ibfk_5` FOREIGN KEY (`CONTENT_ID`) REFERENCES `marketing_contents` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_campaigns`
--

LOCK TABLES `marketing_campaigns` WRITE;
/*!40000 ALTER TABLE `marketing_campaigns` DISABLE KEYS */;
INSERT INTO `marketing_campaigns` VALUES (1,'google',1,-1,'2017-02-11 20:42:04.000','2017-08-27 13:46:42',1,1,1),(2,'cm1',2,3,'2017-08-26 17:19:45.728',NULL,1,25,21),(23,'test campaign update',1,-1,'2017-08-27 13:46:42.747',NULL,1,1,1);
/*!40000 ALTER TABLE `marketing_campaigns` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:49:35
