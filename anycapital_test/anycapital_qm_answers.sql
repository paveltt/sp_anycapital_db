-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `qm_answers`
--

DROP TABLE IF EXISTS `qm_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qm_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The answer''s id.',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The question''s key will be replaced by text''s languages.',
  `question_id` int(11) NOT NULL COMMENT 'Represent the question''s id.',
  `order_id` int(11) NOT NULL COMMENT 'The orders of the answers.',
  `score` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `qm_answers_fk_1_idx` (`question_id`),
  CONSTRAINT `qm_answers_fk_1` FOREIGN KEY (`question_id`) REFERENCES `qm_questions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qm_answers`
--

LOCK TABLES `qm_answers` WRITE;
/*!40000 ALTER TABLE `qm_answers` DISABLE KEYS */;
INSERT INTO `qm_answers` VALUES (1,'Up To €20,000','q1.a1',1,1,0),(2,'€20,001-€50,000','q1.a2',1,2,0),(3,'€50,001-€100,000','q1.a3',1,3,0),(4,'More Than €100,000','q1.a4',1,4,0),(5,'Employment','q2.a1',2,1,0),(6,'Business','q2.a2',2,2,0),(7,'Inheritance','q2.a3',2,3,0),(8,'Investment','q2.a4',2,4,0),(9,'€0-€5,000','q3.a1',3,1,0),(10,'€5,001-€20,000','q3.a2',3,2,0),(11,'€20,001-€50,000','q3.a3',3,3,0),(12,'€50,001-€200,000','q3.a4',3,4,0),(13,'€200,001+','q3.a5',3,5,0),(14,'Daily','q4.a1',4,1,0),(15,'Weekly','q4.a2',4,2,0),(16,'Monthly','q4.a3',4,3,0),(17,'None','q5.a1',5,1,0),(18,'High School Diploma','q5.a2',5,2,0),(19,'Bachelor’s Degree','q5.a3',5,3,0),(20,'Master’s Degree','q5.a4',5,4,0),(21,'Doctorate','q5.a5',5,5,0),(22,'Employed','q6.a1',6,1,0),(23,'Self-Employed','q6.a2',6,2,0),(24,'Retired','q6.a3',6,3,0),(25,'Student','q6.a4',6,4,0),(26,'Unemployed','q6.a5',6,5,0),(27,'Accounting & Finance','q7.a1',7,1,0),(28,'Administration','q7.a2',7,2,0),(29,'Air, Sea & Land Transport','q7.a3',7,3,0),(30,'Architecture, Building and Construction','q7.a4',7,4,0),(31,'Arts & Entertainment','q7.a5',7,5,0),(32,'Consulting and Business Analysis','q7.a6',7,6,0),(33,'Education & Teaching','q7.a7',7,7,0),(34,'Engineering','q7.a8',7,8,0),(35,'Health & Medical Services','q7.a9',7,9,0),(36,'Hospitality & Tourism','q7.a10',7,10,0),(37,'Legal & Compliance','q7.a11',7,11,0),(38,'Sales, Marketing & Advertising','q7.a12',7,12,0),(39,'Science & Research','q7.a13',7,13,0),(40,'Other','q7.a14',7,14,0),(41,'None','q8.a1',8,1,0),(42,'Less Than A Year','q8.a2',8,2,0),(43,'1 To 3 Years','q8.a3',8,3,0),(44,'More Than 3 Years','q8.a4',8,4,0),(45,'Professional Financial Experience','q8.a5',8,5,0),(46,'Yes','q9.a1',9,1,0),(47,'No','q9.a2',9,2,0),(48,'None','q10.a1',10,1,0),(49,'1-20','q10.a2',10,2,0),(50,'21-50','q10.a3',10,3,0),(51,'51-100','q10.a4',10,4,0),(52,'More Than 100','q10.a5',10,5,0),(53,'€0-€20','q11.a1',11,1,0),(54,'€21-€100','q11.a2',11,2,0),(55,'€101-€500','q11.a3',11,3,0),(56,'€501+','q11.a4',11,4,0),(57,'Investment','q12.a1',12,1,0),(58,'Hedging','q12.a2',12,2,0),(59,'Speculation','q12.a3',12,3,0),(60,'Yes','q13.a1',13,1,0),(61,'No','q13.a2',13,2,0),(62,'Yes','q14.a1',14,1,0),(63,'No','q14.a2',14,2,0),(64,'Yes','q15.a1',15,1,0),(65,'No','q15.a2',15,2,0),(66,'Yes','q16.a1',16,1,0),(67,'No','q16.a2',16,2,0),(68,'Yes','q17.a1',17,1,0),(69,'No','q17.a2',17,2,0),(70,'€20,000-€50,000','q18.a1',18,1,0),(71,'€50,001-€200,000','q18.a2',18,2,0),(72,'€200,001-€500,000','q18.a3',18,3,0),(73,'€500,001-€2,000,000','q18.a4',18,4,0),(74,'More than €2,000,000','q18.a5',18,5,0),(75,'€0 – €5,000','q19.a1',19,1,0),(76,'€5,001 – €20,000','q19.a2',19,2,0),(77,'€20,001 – €50,000','q19.a3',19,3,0),(78,'€50,001 – €200,000','q19.a4',19,4,0),(79,'€200,001 +','q19.a5',19,5,0),(80,'Accounting & Finance','q20.a1',20,1,0),(81,'Administration','q20.a2',20,2,0),(82,'Agriculture, Forestry & Fishing','q20.a3',20,3,0),(83,'Air & Sea Transport','q20.a4',20,4,0),(84,'Architecture','q20.a5',20,5,0),(85,'Art & Design','q20.a6',20,6,0),(86,'Arts & Entertainment','q20.a7',20,7,0),(87,'Building and Construction','q20.a8',20,8,0),(88,'Consulting and Business Analysis','q20.a9',20,9,0),(89,'Education & Teaching','q20.a10',20,10,0),(90,'Engineering & Mining','q20.a11',20,11,0),(91,'Environment','q20.a12',20,12,0),(92,'Health & Medical Services','q20.a13',20,13,0),(93,'Hospitality & Tourism','q20.a14',20,14,0),(94,'Human Resources Personnel','q20.a15',20,15,0),(95,'Information Services','q20.a16',20,16,0),(96,'Legal & Compliance','q20.a17',20,17,0),(97,'Management Planning Policy','q20.a18',20,18,0),(98,'Manufacturing Production Logistics','q20.a19',20,19,0),(99,'Media','q20.a20',20,20,0),(100,'Property Real Estate','q20.a21',20,21,0),(101,'Sales, Marketing & Advertising','q20.a22',20,22,0),(102,'Science & Research','q20.a23',20,23,0),(103,'Social Community Services','q20.a24',20,24,0),(104,'Sports Recreation','q20.a25',20,25,0),(105,'Surveying & Valuing','q20.a26',20,26,0),(106,'Trades','q20.a27',20,27,0),(107,'Translating Interpreting','q20.a28',20,28,0),(108,'Other (Please specify)','q20.a29',20,29,0),(109,'None','q.experience.a1',21,1,0),(110,'Less Than A Year','q.experience.a2',21,2,0),(111,'1 To 3 Years','q.experience.a3',21,3,0),(112,'More Than 3 Years','q.experience.a4',21,4,0),(113,'Professional Financial Experience','q.experience.a5',21,5,0),(114,'Daily','q.frequency.a1',22,1,0),(115,'Weekly','q.frequency.a2',22,2,0),(116,'Monthly','q.frequency.a3',22,3,0),(117,'Yearly','q.frequency.a4',22,4,0),(118,'€0 – €5,000','q.annual.volume.a1',23,1,0),(119,'€5,001 – €20,000','q.annual.volume.a2',23,2,0),(120,'€20,001 – €50,000','q.annual.volume.a3',23,3,0),(121,'€50,001 – €200,000','q.annual.volume.a4',23,4,0),(122,'€200,001 +','q.annual.volume.a5',23,5,0),(123,'None','q.experience.a1',24,1,0),(124,'Less Than A Year','q.experience.a2',24,2,0),(125,'1 To 3 Years','q.experience.a3',24,3,0),(126,'More Than 3 Years','q.experience.a4',24,4,0),(127,'Professional Financial Experience','q.experience.a5',24,5,0),(128,'Daily','q.frequency.a1',25,1,0),(129,'Weekly','q.frequency.a2',25,2,0),(130,'Monthly','q.frequency.a3',25,3,0),(131,'Yearly','q.frequency.a4',25,4,0),(132,'€0 – €5,000','q.annual.volume.a1',26,1,0),(133,'€5,001 – €20,000','q.annual.volume.a2',26,2,0),(134,'€20,001 – €50,000','q.annual.volume.a3',26,3,0),(135,'€50,001 – €200,000','q.annual.volume.a4',26,4,0),(136,'€200,001 +','q.annual.volume.a5',26,5,0),(137,'None','q.experience.a1',27,1,0),(138,'Less Than A Year','q.experience.a2',27,2,0),(139,'1 To 3 Years','q.experience.a3',27,3,0),(140,'More Than 3 Years','q.experience.a4',27,4,0),(141,'Professional Financial Experience','q.experience.a5',27,5,0),(142,'Daily','q.frequency.a1',28,1,0),(143,'Weekly','q.frequency.a2',28,2,0),(144,'Monthly','q.frequency.a3',28,3,0),(145,'Yearly','q.frequency.a4',28,4,0),(146,'€0 – €5,000','q.annual.volume.a1',29,1,0),(147,'€5,001 – €20,000','q.annual.volume.a2',29,2,0),(148,'€20,001 – €50,000','q.annual.volume.a3',29,3,0),(149,'€50,001 – €200,000','q.annual.volume.a4',29,4,0),(150,'€200,001 +','q.annual.volume.a5',29,5,0),(151,'None','q.experience.a1',30,1,0),(152,'Less Than A Year','q.experience.a2',30,2,0),(153,'1 To 3 Years','q.experience.a3',30,3,0),(154,'More Than 3 Years','q.experience.a4',30,4,0),(155,'Professional Financial Experience','q.experience.a5',30,5,0),(156,'Daily','q.frequency.a1',31,1,0),(157,'Weekly','q.frequency.a2',31,2,0),(158,'Monthly','q.frequency.a3',31,3,0),(159,'Yearly','q.frequency.a4',31,4,0),(160,'€0 – €5,000','q.annual.volume.a1',32,1,0),(161,'€5,001 – €20,000','q.annual.volume.a2',32,2,0),(162,'€20,001 – €50,000','q.annual.volume.a3',32,3,0),(163,'€50,001 – €200,000','q.annual.volume.a4',32,4,0),(164,'€200,001 +','q.annual.volume.a5',32,5,0),(165,'None','q.experience.a1',33,1,0),(166,'Less Than A Year','q.experience.a2',33,2,0),(167,'1 To 3 Years','q.experience.a3',33,3,0),(168,'More Than 3 Years','q.experience.a4',33,4,0),(169,'Professional Financial Experience','q.experience.a5',33,5,0),(170,'Daily','q.frequency.a1',34,1,0),(171,'Weekly','q.frequency.a2',34,2,0),(172,'Monthly','q.frequency.a3',34,3,0),(173,'Yearly','q.frequency.a4',34,4,0),(174,'€0 – €5,000','q.annual.volume.a1',35,1,0),(175,'€5,001 – €20,000','q.annual.volume.a2',35,2,0),(176,'€20,001 – €50,000','q.annual.volume.a3',35,3,0),(177,'€50,001 – €200,000','q.annual.volume.a4',35,4,0),(178,'€200,001 +','q.annual.volume.a5',35,5,0),(179,'None','q.experience.a1',36,1,0),(180,'Less Than A Year','q.experience.a2',36,2,0),(181,'1 To 3 Years','q.experience.a3',36,3,0),(182,'More Than 3 Years','q.experience.a4',36,4,0),(183,'Professional Financial Experience','q.experience.a5',36,5,0),(184,'Daily','q.frequency.a1',37,1,0),(185,'Weekly','q.frequency.a2',37,2,0),(186,'Monthly','q.frequency.a3',37,3,0),(187,'Yearly','q.frequency.a4',37,4,0),(188,'€0 – €5,000','q.annual.volume.a1',38,1,0),(189,'€5,001 – €20,000','q.annual.volume.a2',38,2,0),(190,'€20,001 – €50,000','q.annual.volume.a3',38,3,0),(191,'€50,001 – €200,000','q.annual.volume.a4',38,4,0),(192,'€200,001 +','q.annual.volume.a5',38,5,0),(193,'$11,000','q39.a1',39,1,0),(194,'$12,000','q39.a2',39,2,0),(195,'$15,000','q39.a3',39,3,0),(196,'$8,000','q39.a4',39,4,0),(197,'-15%','q40.a1',40,1,0),(198,'+1.15%','q40.a2',40,2,0),(199,'+115%','q40.a3',40,3,0),(200,'+15%','q40.a4',40,4,0),(201,'$5,000','q41.a1',41,1,0),(202,'$9,000','q41.a2',41,2,0),(203,'$10,000','q41.a3',41,3,0),(204,'$11,000','q41.a4',41,4,0),(205,'Yes','q.yes.a1',42,1,0),(206,'No','q.no.a2',42,2,0),(207,'Yes','q.yes.a1',43,1,0),(208,'No','q.no.a2',43,2,0),(209,'Yes','q.yes.a1',44,1,0),(210,'No','q.no.a2',44,2,0);
/*!40000 ALTER TABLE `qm_answers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:50:43
