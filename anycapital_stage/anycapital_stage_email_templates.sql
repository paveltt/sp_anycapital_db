-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_templates` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `action_id` smallint(6) NOT NULL COMMENT 'Foreign key to email_actions.',
  `language_id` smallint(6) NOT NULL COMMENT 'Foreign key to languages.',
  `template_id` varchar(100) NOT NULL COMMENT 'The template id which defined in Sendgrid system.',
  `is_active` binary(1) NOT NULL DEFAULT '1' COMMENT 'Represent if the record is active or not.',
  PRIMARY KEY (`id`),
  KEY `fk_et_action_id_idx` (`action_id`),
  KEY `fk_et_language_id_idx` (`language_id`),
  CONSTRAINT `fk_et_action_id` FOREIGN KEY (`action_id`) REFERENCES `email_actions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_et_language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_templates`
--

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
INSERT INTO `email_templates` VALUES (5,1,2,'60acb4ca-37b8-44d7-b533-5540e787186a','1'),(6,2,2,'dc7cb311-be82-4670-98b7-cd610584c842','1'),(7,8,2,'105bd4c1-368d-4a8d-b175-e4790e12c518','1'),(8,7,2,'67729213-bd3e-4a33-902c-aade1010fee8','1'),(9,9,2,'5a6464bb-db94-4299-8228-e68c36901b90','1'),(10,10,2,'f5bd1b94-52f0-4ad5-b25c-b30591cb5a90','1'),(11,11,2,'474875ca-27fa-45f0-8c93-bc7f253cf744','1'),(12,12,2,'74bb1401-db3d-4a30-90ed-506c74b78ca5','1'),(13,13,2,'f1f89c6f-9308-46bc-b9e6-fbbf50faea21','1'),(14,1,1,'f37e398e-eb1e-4975-90aa-ca5a3875f1ef','1'),(15,2,1,'17276f62-2b9d-4159-a961-49504d4e0b83','1'),(16,8,1,'e68b7999-d3e6-40cf-bf8d-b15d1f4c917e','1'),(17,7,1,'69e8e383-8b66-496d-8f1f-64abd3d30806','1'),(18,9,1,'f1053a07-de96-42f5-a8c9-ee2e2c0bb748','1'),(19,10,1,'7e73604b-5f92-462a-8bb0-2d9686fc9182','1'),(20,11,1,'1f1ab230-0467-412e-a6c1-89f793a7108d','1'),(21,12,1,'e486c646-c5af-4856-a49e-819f2fc0ecb9','1'),(22,13,1,'20a7dacd-7f4a-45f7-b188-6d00f996a71a','1');
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:44:47
