-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marketing_campaigns`
--

DROP TABLE IF EXISTS `marketing_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketing_campaigns` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Campaign''s id',
  `NAME` varchar(100) NOT NULL COMMENT 'Campaign''s name',
  `SOURCE_ID` smallint(6) DEFAULT NULL COMMENT 'Foreign key to MARKETING_SOURCES',
  `WRITER_ID` smallint(6) NOT NULL COMMENT 'Foreign key to WRITERS',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'Campaign''s time created',
  `TIME_MODIFIED` timestamp NULL DEFAULT NULL COMMENT 'Campaign''s time updated',
  `DOMAIN_ID` smallint(6) DEFAULT NULL COMMENT 'Foreign key to MARKETING_DOMAINS',
  `LANDING_PAGE_ID` smallint(6) DEFAULT NULL COMMENT 'Foreign key to MARKETING_LANDING_PAGES',
  `CONTENT_ID` smallint(6) DEFAULT NULL COMMENT 'Foreign key to MARKETING_CONTENTS',
  PRIMARY KEY (`ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  KEY `SOURCE_ID` (`SOURCE_ID`),
  KEY `DOMAIN_ID` (`DOMAIN_ID`),
  KEY `LANDING_PAGE_ID` (`LANDING_PAGE_ID`),
  KEY `CONTENT_ID` (`CONTENT_ID`),
  CONSTRAINT `marketing_campaigns_ibfk_1` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `marketing_campaigns_ibfk_2` FOREIGN KEY (`SOURCE_ID`) REFERENCES `marketing_sources` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `marketing_campaigns_ibfk_3` FOREIGN KEY (`DOMAIN_ID`) REFERENCES `marketing_domains` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `marketing_campaigns_ibfk_4` FOREIGN KEY (`LANDING_PAGE_ID`) REFERENCES `marketing_landing_pages` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `marketing_campaigns_ibfk_5` FOREIGN KEY (`CONTENT_ID`) REFERENCES `marketing_contents` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_campaigns`
--

LOCK TABLES `marketing_campaigns` WRITE;
/*!40000 ALTER TABLE `marketing_campaigns` DISABLE KEYS */;
INSERT INTO `marketing_campaigns` VALUES (1,'default',1,3,'2017-08-03 16:27:04.901',NULL,1,1,1),(4,'selenium automation',1,6,'2017-08-09 06:17:33.871',NULL,1,1,1),(5,'test-before-lunch',1,3,'2017-09-03 09:14:51.363','2017-09-03 09:15:32',1,1,1),(6,'Anycapital_Mailer_v_e',1,104,'2017-09-06 08:29:06.932',NULL,1,2,2),(7,'anycapital_mailer_v_a',1,104,'2017-09-06 08:31:01.832',NULL,1,2,3),(8,'anycapital_mailer_v_b',1,3,'2017-09-13 06:35:51.068',NULL,1,2,4),(9,'anycapital_mailer_v_c',1,3,'2017-09-13 06:36:34.918',NULL,1,2,5),(10,'AO_Popup',3,104,'2017-09-25 09:32:13.949',NULL,1,1,6),(11,'Anycapital_Woman_SG_LP',1,104,'2017-09-27 07:33:23.821','2017-09-27 08:05:47',1,2,7),(13,'Anycapital_Woman_SG_Product',1,104,'2017-09-27 08:06:37.889',NULL,1,4,7),(14,'SMS_promotion_AO_users_EN',2,3,'2017-10-02 09:32:16.192','2017-10-17 11:21:56',1,2,8),(15,'SMS_promotion_AO_users_ES',2,3,'2017-10-17 11:22:19.684',NULL,1,2,9),(16,'SMS_promotion_AO_users_DE',2,3,'2017-10-17 11:22:38.141',NULL,1,2,10);
/*!40000 ALTER TABLE `marketing_campaigns` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:15
