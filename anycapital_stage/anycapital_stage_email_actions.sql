-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `email_actions`
--

DROP TABLE IF EXISTS `email_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_actions` (
  `id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `display_name` varchar(60) DEFAULT NULL,
  `class_path` varchar(100) DEFAULT NULL COMMENT 'path for handler class',
  `is_display` binary(1) DEFAULT '1' COMMENT 'Display on CRM when equals to 1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_actions`
--

LOCK TABLES `email_actions` WRITE;
/*!40000 ALTER TABLE `email_actions` DISABLE KEYS */;
INSERT INTO `email_actions` VALUES (1,'register','email.action.register','capital.any.service.base.emailAction.handler.RegisterEmailActionHandler','1'),(2,'forget password','email.action.forget.password','capital.any.service.base.emailAction.handler.ForgetPasswordEmailActionHandler','1'),(3,'market daily history job',NULL,NULL,'0'),(4,'product settle',NULL,NULL,'0'),(5,'change product status',NULL,NULL,'0'),(6,'daily report',NULL,NULL,'0'),(7,'insert investment open','email.action.investment.open','capital.any.service.base.emailAction.handler.InvestmentOpenEmailActionHandler','1'),(8,'insert investment pending','email.action.investment.pending','capital.any.service.base.emailAction.handler.InvestmentPendingEmailActionHandler','1'),(9,'docs approved','email.action.docs.approved','capital.any.service.base.emailAction.handler.GeneralEmailActionHandler','1'),(10,'Deposit confirmation bank wire','email.action.deposit.confirmation','capital.any.service.base.emailAction.handler.TransactionWireEmailActionHandler','1'),(11,'Risk Disclosure','email.action.risk.disclosure','capital.any.service.base.emailAction.handler.GeneralEmailActionHandler','1'),(12,'Receipt asking docs','email.action.receipt.asking.docs','capital.any.service.base.emailAction.handler.GeneralEmailActionHandler','1'),(13,'Documents rejected','email.action.receipt.documents.rejected','capital.any.service.base.emailAction.handler.GeneralEmailActionHandler','1');
/*!40000 ALTER TABLE `email_actions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:46:06
