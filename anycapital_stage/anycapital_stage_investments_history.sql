-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `investments_history`
--

DROP TABLE IF EXISTS `investments_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investments_history` (
  `INVESTMNT_ID` bigint(20) NOT NULL,
  `INVESTMENT_STATUS_ID` smallint(6) NOT NULL,
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `WRITER_ID` smallint(6) DEFAULT NULL,
  `ACTION_SOURCE_ID` tinyint(4) NOT NULL,
  `SERVER_NAME` varchar(50) NOT NULL,
  `LOGIN_ID` bigint(20) DEFAULT NULL COMMENT 'the login id of the user that done that action - null if its job/backend',
  `IP` varchar(20) DEFAULT NULL COMMENT 'the  ip of the user that done that action - null if its job',
  PRIMARY KEY (`INVESTMNT_ID`,`INVESTMENT_STATUS_ID`),
  KEY `INVESTMENT_STATUS_ID` (`INVESTMENT_STATUS_ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  KEY `ACTION_SOURCE_ID` (`ACTION_SOURCE_ID`),
  KEY `LOGIN_ID` (`LOGIN_ID`),
  CONSTRAINT `investments_history_ibfk_1` FOREIGN KEY (`INVESTMNT_ID`) REFERENCES `investments` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_history_ibfk_2` FOREIGN KEY (`INVESTMENT_STATUS_ID`) REFERENCES `investment_statuses` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_history_ibfk_3` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_history_ibfk_4` FOREIGN KEY (`ACTION_SOURCE_ID`) REFERENCES `action_source` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investments_history_ibfk_5` FOREIGN KEY (`LOGIN_ID`) REFERENCES `logins` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investments_history`
--

LOCK TABLES `investments_history` WRITE;
/*!40000 ALTER TABLE `investments_history` DISABLE KEYS */;
INSERT INTO `investments_history` VALUES (10000,2,'2017-08-10 07:33:08.371',NULL,1,'ww00.anycapital.com\n',20,'199.203.251.26'),(10001,2,'2017-09-03 11:29:57.361',NULL,1,'ww00.anycapital.com\n',25,'199.203.251.26'),(10002,2,'2017-09-03 12:33:41.345',NULL,1,'ww00.anycapital.com\n',26,'199.203.251.26'),(10003,1,'2017-09-04 08:10:44.883',NULL,1,'ww00.anycapital.com\n',40,'199.203.251.26'),(10003,2,'2017-09-04 08:13:40.293',3,2,'crm1.anycapital.com\n',NULL,NULL),(10003,4,'2017-09-04 08:16:11.923',3,2,'crm1.anycapital.com\n',NULL,'199.203.251.26'),(10004,1,'2017-09-04 08:11:19.064',NULL,1,'ww00.anycapital.com\n',40,'199.203.251.26'),(10004,2,'2017-09-04 08:13:40.305',3,2,'crm1.anycapital.com\n',NULL,NULL),(10005,2,'2017-09-04 08:23:27.823',NULL,1,'ww00.anycapital.com\n',42,'199.203.251.26'),(10006,1,'2017-09-05 07:58:08.923',NULL,1,'ww00.anycapital.com\n',48,'199.203.251.26'),(10007,2,'2017-09-07 12:35:41.759',NULL,1,'ww00.anycapital.com\n',107,'199.203.251.26'),(10008,2,'2017-09-07 12:37:20.770',NULL,1,'ww00.anycapital.com\n',108,'199.203.251.26'),(10009,2,'2017-09-07 12:40:00.362',NULL,1,'ww00.anycapital.com\n',109,'199.203.251.26'),(10010,2,'2017-09-07 12:43:10.571',NULL,1,'ww00.anycapital.com\n',110,'199.203.251.26'),(10011,2,'2017-09-10 06:45:29.936',NULL,1,'ww00.anycapital.com\n',119,'199.203.251.26'),(10012,2,'2017-09-10 06:47:44.941',NULL,1,'ww00.anycapital.com\n',120,'199.203.251.26'),(10013,2,'2017-09-10 08:33:51.175',NULL,1,'ww00.anycapital.com\n',129,'199.203.251.26'),(10014,2,'2017-09-10 09:10:15.137',NULL,1,'ww00.anycapital.com\n',131,'199.203.251.26'),(10015,2,'2017-09-10 09:14:23.498',NULL,1,'ww00.anycapital.com\n',132,'199.203.251.26'),(10016,2,'2017-09-18 06:34:24.130',NULL,1,'ww00.anycapital.com\n',160,'199.203.251.26'),(10027,2,'2017-11-16 16:02:32.380',NULL,1,'IL-PC1078\n',216,'0:0:0:0:0:0:0:1');
/*!40000 ALTER TABLE `investments_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:46:03
