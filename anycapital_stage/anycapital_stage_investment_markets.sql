-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `investment_markets`
--

DROP TABLE IF EXISTS `investment_markets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investment_markets` (
  `INVESTMENT_ID` bigint(20) NOT NULL,
  `MARKET_ID` smallint(6) NOT NULL,
  `PRICE` double NOT NULL COMMENT 'the price of the market when user insert investment',
  PRIMARY KEY (`INVESTMENT_ID`,`MARKET_ID`),
  KEY `MARKET_ID` (`MARKET_ID`),
  CONSTRAINT `investment_markets_ibfk_1` FOREIGN KEY (`MARKET_ID`) REFERENCES `markets` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `investment_markets_ibfk_2` FOREIGN KEY (`INVESTMENT_ID`) REFERENCES `investments` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investment_markets`
--

LOCK TABLES `investment_markets` WRITE;
/*!40000 ALTER TABLE `investment_markets` DISABLE KEYS */;
INSERT INTO `investment_markets` VALUES (10000,13,14.88),(10001,9,171.04),(10002,28,355.4),(10003,28,355.4),(10004,37,37.36),(10005,37,37.36),(10006,34,47.115),(10007,11,16.83),(10008,11,16.83),(10009,11,16.83),(10010,11,16.83),(10011,11,17.45),(10012,11,17.45),(10013,11,17.43),(10014,11,17.43),(10015,11,17.43),(10016,11,18.01),(10027,11,20.52);
/*!40000 ALTER TABLE `investment_markets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:41
