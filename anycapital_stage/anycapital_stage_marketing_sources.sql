-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marketing_sources`
--

DROP TABLE IF EXISTS `marketing_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketing_sources` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Source''s id',
  `NAME` varchar(100) NOT NULL COMMENT 'Source''s name',
  `WRITER_ID` smallint(6) NOT NULL COMMENT 'Foreign key to WRITERS',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'Record''s time created',
  `TIME_MODIFIED` timestamp NULL DEFAULT NULL COMMENT 'Record''s time updated',
  PRIMARY KEY (`ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  CONSTRAINT `marketing_sources_ibfk_1` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_sources`
--

LOCK TABLES `marketing_sources` WRITE;
/*!40000 ALTER TABLE `marketing_sources` DISABLE KEYS */;
INSERT INTO `marketing_sources` VALUES (1,'email',3,'2017-08-03 16:09:57.706',NULL),(2,'sms',3,'2017-09-24 06:32:42.914',NULL),(3,'popup',3,'2017-09-24 06:32:52.164',NULL),(4,'push',3,'2017-10-02 09:30:59.350',NULL);
/*!40000 ALTER TABLE `marketing_sources` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:04
