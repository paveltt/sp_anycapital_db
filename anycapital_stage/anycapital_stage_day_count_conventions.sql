-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `day_count_conventions`
--

DROP TABLE IF EXISTS `day_count_conventions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `day_count_conventions` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL COMMENT 'we use this name as convention string for the formula',
  `DISPLAY_NAME` varchar(40) NOT NULL COMMENT 'messages properties key name',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='In finance, a day count convention determines how interest accrues over time for a variety of investments, including bonds, notes, loans, mortgages, medium-term notes, swaps, and forward rate agreements (FRAs).\nhttps://en.wikipedia.org/wiki/Day_count_convention';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `day_count_conventions`
--

LOCK TABLES `day_count_conventions` WRITE;
/*!40000 ALTER TABLE `day_count_conventions` DISABLE KEYS */;
INSERT INTO `day_count_conventions` VALUES (1,'act/act isda','DAY.COUNT.CONVENTIONS.1'),(2,'act/365','DAY.COUNT.CONVENTIONS.2'),(3,'act/360','DAY.COUNT.CONVENTIONS.3'),(4,'30U/360','day.count.conventions.4');
/*!40000 ALTER TABLE `day_count_conventions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:19
