-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `qm_answers`
--

DROP TABLE IF EXISTS `qm_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qm_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The answer''s id.',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'The question''s key will be replaced by text''s languages.',
  `question_id` int(11) NOT NULL COMMENT 'Represent the question''s id.',
  `order_id` int(11) NOT NULL COMMENT 'The orders of the answers.',
  `score` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `qm_answers_fk_1_idx` (`question_id`),
  CONSTRAINT `qm_answers_fk_1` FOREIGN KEY (`question_id`) REFERENCES `qm_questions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qm_answers`
--

LOCK TABLES `qm_answers` WRITE;
/*!40000 ALTER TABLE `qm_answers` DISABLE KEYS */;
INSERT INTO `qm_answers` VALUES (1,'Up To €20,000','q1.a1',1,1,1),(2,'€20,001-€50,000','q1.a2',1,2,0),(3,'€50,001-€100,000','q1.a3',1,3,0),(4,'More Than €100,000','q1.a4',1,4,0),(5,'Employment','q2.a1',2,1,0),(6,'Business','q2.a2',2,2,0),(7,'Inheritance','q2.a3',2,3,0),(8,'Investment','q2.a4',2,4,0),(9,'€0-€5,000','q3.a1',3,1,0),(10,'€5,001-€20,000','q3.a2',3,2,0),(11,'€20,001-€50,000','q3.a3',3,3,0),(12,'€50,001-€200,000','q3.a4',3,4,0),(13,'€200,001+','q3.a5',3,5,0),(14,'Daily','q4.a1',4,1,0),(15,'Weekly','q4.a2',4,2,0),(16,'Monthly','q4.a3',4,3,0),(17,'None','q5.a1',5,1,0),(18,'High School Diploma','q5.a2',5,2,0),(19,'Bachelor’s Degree','q5.a3',5,3,0),(20,'Master’s Degree','q5.a4',5,4,0),(21,'Doctorate','q5.a5',5,5,0),(22,'Employed','q6.a1',6,1,0),(23,'Self-Employed','q6.a2',6,2,0),(24,'Retired','q6.a3',6,3,0),(25,'Student','q6.a4',6,4,0),(26,'Unemployed','q6.a5',6,5,10),(27,'Accounting & Finance','q7.a1',7,1,0),(28,'Administration','q7.a2',7,2,0),(29,'Air, Sea & Land Transport','q7.a3',7,3,0),(30,'Architecture, Building and Construction','q7.a4',7,4,0),(31,'Arts & Entertainment','q7.a5',7,5,0),(32,'Consulting and Business Analysis','q7.a6',7,6,0),(33,'Education & Teaching','q7.a7',7,7,0),(34,'Engineering','q7.a8',7,8,0),(35,'Health & Medical Services','q7.a9',7,9,0),(36,'Hospitality & Tourism','q7.a10',7,10,0),(37,'Legal & Compliance','q7.a11',7,11,0),(38,'Sales, Marketing & Advertising','q7.a12',7,12,0),(39,'Science & Research','q7.a13',7,13,0),(40,'Other','q7.a14',7,14,0),(41,'None','q8.a1',8,1,100),(42,'Less Than A Year','q8.a2',8,2,0),(43,'1 To 3 Years','q8.a3',8,3,0),(44,'More Than 3 Years','q8.a4',8,4,0),(45,'Professional Financial Experience','q8.a5',8,5,0),(46,'Yes','q9.a1',9,1,0),(47,'No','q9.a2',9,2,1000),(48,'None','q10.a1',10,1,0),(49,'1-20','q10.a2',10,2,0),(50,'21-50','q10.a3',10,3,0),(51,'51-100','q10.a4',10,4,0),(52,'More Than 100','q10.a5',10,5,0),(53,'€0-€20','q11.a1',11,1,0),(54,'€21-€100','q11.a2',11,2,0),(55,'€101-€500','q11.a3',11,3,0),(56,'€501+','q11.a4',11,4,0),(57,'Investment','q12.a1',12,1,0),(58,'Hedging','q12.a2',12,2,0),(59,'Speculation','q12.a3',12,3,0),(60,'Yes','q13.a1',13,1,0),(61,'No','q13.a2',13,2,0),(62,'Yes','q14.a1',14,1,0),(63,'No','q14.a2',14,2,0),(64,'Yes','q15.a1',15,1,0),(65,'No','q15.a2',15,2,0),(66,'Yes','q16.a1',16,1,0),(67,'No','q16.a2',16,2,0),(68,'Yes','q17.a1',17,1,0),(69,'No','q17.a2',17,2,0);
/*!40000 ALTER TABLE `qm_answers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:46:20
