-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PRODUCT_STATUS_ID` smallint(6) NOT NULL,
  `PRODUCT_TYPE_ID` smallint(6) NOT NULL,
  `ISSUE_PRICE` smallint(6) NOT NULL COMMENT 'percentage of the Denomination',
  `CURRENCY_ID` smallint(6) NOT NULL COMMENT 'currency of Denomination',
  `DENOMINATION` int(11) NOT NULL COMMENT 'amount of one denomination (minimum investment)',
  `PRODUCT_COUPON_FREQUENCY_ID` smallint(6) NOT NULL COMMENT 'when to give user coupon only for user information',
  `DAY_COUNT_CONVENTION_ID` smallint(6) DEFAULT NULL,
  `MAX_YIELD` smallint(6) DEFAULT NULL COMMENT 'maximum profit effective profit',
  `MAX_YIELD_P_A` smallint(6) DEFAULT NULL COMMENT 'maximum profit yearly',
  `LEVEL_OF_PROTECTION` smallint(6) DEFAULT NULL COMMENT 'how much percentage of user investment amount is protected user cant get less than this amount. (maximum u  can lose 1-level of protection)',
  `LEVEL_OF_PARTICIPATION` smallint(6) DEFAULT NULL COMMENT 'percentage of the level change',
  `STRIKE_LEVEL` smallint(6) DEFAULT NULL COMMENT 'percentage of the market price at the start of the product for formula result',
  `BONUS_LEVEL` smallint(6) DEFAULT NULL COMMENT 'percentage of the Denomination at the start of the product for formula result',
  `SUBSCRIPTION_START_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'start date to subscribe',
  `SUBSCRIPTION_END_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'end date to subscribe',
  `INITIAL_FIXING_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date where we fix the initial level of the underlying',
  `ISSUE_DATE` timestamp NULL DEFAULT NULL COMMENT 'Date of the issuance of the product usually at the Exchange or Clearance house in case there is an issuance otherwise this date is similar to the initial fixing date',
  `FIRST_EXCHANGE_TRADING_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'first day where we take into account the underlying movement',
  `LAST_TRADING_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'last day of trading of the underlying that account for the product',
  `FINAL_FIXING_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date where we fix the final fixing level for settlement calculation',
  `REDEMPTION_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'when we need to give money to customers',
  `IS_SUSPEND` binary(1) NOT NULL DEFAULT '0' COMMENT '1 if this market is currently suspend',
  `IS_QUANTO` binary(1) NOT NULL DEFAULT '0' COMMENT '1 if the markets have different currency from the denomination',
  `PRODUCT_INSURANCE_TYPE_ID` smallint(6) DEFAULT NULL COMMENT 'type of the Issuer Insurance from product_Issuer_Insurance_types  can be null',
  `BID` double DEFAULT NULL COMMENT 'when product is in Secondary state user can sell his investment with this bid percentage of the domination',
  `ASK` double DEFAULT NULL COMMENT 'when product is in Secondary state user can buy investment with this ask percentage of the domination',
  `FORMULA_RESULT` double DEFAULT NULL COMMENT 'when settle we take the formula result to calculate the investment result',
  `BOND_FLOOR_AT_ISSUANCE` double DEFAULT NULL COMMENT 'structured product include a zero coupon bond + an option strategy\n    this is the zero coupon face amount',
  `ISSUE_SIZE` bigint(20) DEFAULT NULL COMMENT 'max amount that we accept subscription',
  `PRIORITY` int(11) DEFAULT NULL COMMENT 'the priority of the product in home page and products page',
  `CAP_LEVEL` double DEFAULT NULL COMMENT '0 if no cap level , percentage that represent from where the client stop participating to the upside',
  `PRODUCT_MATURITY_ID` smallint(6) NOT NULL COMMENT 'in product pages we need to display maturity in term of Tenor',
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_TYPE_ID` (`PRODUCT_TYPE_ID`),
  KEY `PRODUCT_STATUS_ID` (`PRODUCT_STATUS_ID`),
  KEY `DAY_COUNT_CONVENTION_ID` (`DAY_COUNT_CONVENTION_ID`),
  KEY `PRODUCT_COUPON_FREQUENCY_ID` (`PRODUCT_COUPON_FREQUENCY_ID`),
  KEY `PRODUCT_MATURITY_ID` (`PRODUCT_MATURITY_ID`),
  KEY `CURRENCY_ID` (`CURRENCY_ID`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`PRODUCT_TYPE_ID`) REFERENCES `product_types` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `products_ibfk_2` FOREIGN KEY (`PRODUCT_STATUS_ID`) REFERENCES `product_statuses` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `products_ibfk_3` FOREIGN KEY (`DAY_COUNT_CONVENTION_ID`) REFERENCES `day_count_conventions` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `products_ibfk_4` FOREIGN KEY (`PRODUCT_COUPON_FREQUENCY_ID`) REFERENCES `product_coupon_frequencies` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `products_ibfk_5` FOREIGN KEY (`PRODUCT_MATURITY_ID`) REFERENCES `product_maturities` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `products_ibfk_6` FOREIGN KEY (`CURRENCY_ID`) REFERENCES `currencies` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'2017-08-03 15:24:58.252',4,1,100,3,100000,1,3,100,100,100,100,0,0,'2017-08-01 00:00:00','2017-08-06 23:59:59','2017-08-08 00:00:00','2017-08-10 00:00:00','2017-08-11 00:00:00','2017-08-16 00:00:00','2017-08-15 00:00:00','2017-08-18 00:00:00','1','0',1,NULL,NULL,NULL,100,10000,1,0,4),(2,'2017-08-07 10:41:14.123',3,5,100,2,100000,6,4,0,0,0,0,0,0,'2017-08-07 00:00:00','2017-08-28 23:59:59','2017-08-28 00:00:00','2017-08-28 00:00:00','2017-08-28 00:00:00','2018-08-28 00:00:00','2018-08-28 00:00:00','2018-08-30 00:00:00','1','0',0,NULL,NULL,NULL,100,200000000,2,0,4),(1000,'2017-08-07 11:09:39.691',3,2,100,3,100000,1,4,0,0,94,150,0,0,'2017-08-07 00:00:00','2017-08-28 23:59:59','2017-08-28 00:00:00','2017-08-28 00:00:00','2017-08-28 00:00:00','2018-08-28 00:00:00','2018-08-28 00:00:00','2018-08-30 00:00:00','1','0',0,NULL,NULL,NULL,94,100000000,1000,0,4),(1001,'2017-08-29 14:17:47.649',3,4,100,2,100000,5,4,0,0,0,0,80,0,'2017-08-29 00:00:00','2017-09-29 23:59:59','2017-09-29 00:00:00','2017-09-29 00:00:00','2017-09-29 00:00:00','2019-09-30 00:00:00','2019-09-30 00:00:00','2019-10-02 00:00:00','1','0',0,NULL,NULL,NULL,100,100000000,1001,0,6),(1002,'2017-08-29 14:34:50.416',2,1,100,2,100000,1,4,0,0,93,100,0,0,'2017-11-13 00:00:00','2017-12-13 23:59:59','2017-12-13 00:00:00','2017-12-13 00:00:00','2017-12-14 00:00:00','2018-12-13 00:00:00','2018-12-13 00:00:00','2018-12-17 00:00:00','0','1',0,NULL,NULL,NULL,91.46,200000000,1002,0,4),(1003,'2017-09-03 11:56:01.619',2,2,100,2,100000,1,4,0,0,95,225,0,0,'2017-11-13 00:00:00','2017-12-13 23:59:59','2017-12-13 00:00:00','2017-12-13 00:00:00','2017-12-14 00:00:00','2018-12-13 00:00:00','2018-12-13 00:00:00','2018-12-17 00:00:00','0','0',0,NULL,NULL,NULL,93.43,100000000,1003,0,4),(1004,'2017-09-04 07:58:33.333',2,8,100,2,100000,1,NULL,0,0,0,145,0,0,'2017-11-13 00:00:00','2017-12-13 23:59:59','2017-12-13 00:00:00','2017-12-13 00:00:00','2017-12-14 00:00:00','2020-12-10 00:00:00','2020-12-10 00:00:00','2020-12-15 00:00:00','0','0',0,NULL,NULL,NULL,95.34,200000000,1004,0,7),(1005,'2017-09-04 09:02:16.558',2,5,100,3,100000,5,4,0,0,0,0,0,0,'2017-11-13 00:00:00','2017-12-13 23:59:59','2017-12-13 00:00:00','2017-12-13 00:00:00','2017-12-14 00:00:00','2018-12-13 00:00:00','2018-12-13 00:00:00','2018-12-17 00:00:00','0','0',0,NULL,NULL,NULL,100,100000000,1005,0,4),(1006,'2017-09-04 09:06:20.857',2,5,100,3,100000,5,4,0,0,0,0,0,0,'2017-11-13 00:00:00','2017-12-13 23:59:59','2017-12-13 00:00:00','2017-12-13 00:00:00','2017-12-14 00:00:00','2018-12-13 00:00:00','2018-12-13 00:00:00','2018-12-17 00:00:00','0','0',0,NULL,NULL,NULL,100,100000000,1006,0,4),(1007,'2017-09-04 14:12:54.672',2,4,100,2,100000,5,4,0,0,0,0,80,0,'2017-11-13 00:00:00','2017-12-13 23:59:59','2017-12-13 00:00:00','2017-12-13 00:00:00','2017-12-14 00:00:00','2020-12-10 00:00:00','2020-12-10 00:00:00','2020-12-15 00:00:00','0','0',0,NULL,NULL,NULL,94.34,100000000,1007,0,7);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:26
