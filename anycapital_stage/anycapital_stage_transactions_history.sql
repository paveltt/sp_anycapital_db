-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transactions_history`
--

DROP TABLE IF EXISTS `transactions_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions_history` (
  `TRANSACTION_ID` bigint(20) NOT NULL COMMENT 'reference',
  `TRANSACTION_STATUS_ID` smallint(6) NOT NULL COMMENT 'reference',
  `WRITER_ID` smallint(6) DEFAULT NULL COMMENT 'reference',
  `SERVER_NAME` varchar(50) NOT NULL COMMENT 'The server from which the operation was carried out',
  `ACTION_SOURCE_ID` tinyint(4) NOT NULL COMMENT 'reference',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `LOGIN_ID` bigint(20) DEFAULT NULL COMMENT 'the login id of the user that made this action 0 if its job',
  KEY `TRANSACTION_STATUS_ID` (`TRANSACTION_STATUS_ID`),
  KEY `TRANSACTION_ID` (`TRANSACTION_ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  KEY `ACTION_SOURCE_ID` (`ACTION_SOURCE_ID`),
  KEY `LOGIN_ID` (`LOGIN_ID`),
  CONSTRAINT `transactions_history_ibfk_1` FOREIGN KEY (`TRANSACTION_STATUS_ID`) REFERENCES `transaction_statuses` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `transactions_history_ibfk_2` FOREIGN KEY (`TRANSACTION_ID`) REFERENCES `transactions` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `transactions_history_ibfk_3` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `transactions_history_ibfk_4` FOREIGN KEY (`ACTION_SOURCE_ID`) REFERENCES `action_source` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `transactions_history_ibfk_5` FOREIGN KEY (`LOGIN_ID`) REFERENCES `logins` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions_history`
--

LOCK TABLES `transactions_history` WRITE;
/*!40000 ALTER TABLE `transactions_history` DISABLE KEYS */;
INSERT INTO `transactions_history` VALUES (10000,1,6,'crm1.anycapital.com\n',2,'2017-08-09 06:29:54.539',NULL),(10000,2,6,'crm1.anycapital.com\n',2,'2017-08-09 06:29:54.588',NULL),(10001,1,3,'crm1.anycapital.com\n',2,'2017-08-10 07:30:42.098',NULL),(10001,2,3,'crm1.anycapital.com\n',2,'2017-08-10 07:30:42.105',NULL),(10002,1,3,'crm1.anycapital.com\n',2,'2017-09-03 10:54:46.489',NULL),(10002,4,3,'crm1.anycapital.com\n',2,'2017-09-03 10:54:46.514',NULL),(10002,2,3,'crm1.anycapital.com\n',2,'2017-09-03 10:55:22.242',NULL),(10003,1,3,'crm1.anycapital.com\n',2,'2017-09-04 08:12:56.587',NULL),(10003,4,3,'crm1.anycapital.com\n',2,'2017-09-04 08:12:56.596',NULL),(10003,2,3,'crm1.anycapital.com\n',2,'2017-09-04 08:13:40.274',NULL),(10004,1,3,'crm1.anycapital.com\n',2,'2017-09-17 14:02:58.497',NULL),(10004,4,3,'crm1.anycapital.com\n',2,'2017-09-17 14:02:58.522',NULL),(10004,2,3,'crm1.anycapital.com\n',2,'2017-09-17 14:03:38.153',NULL),(10005,1,3,'crm1.anycapital.com\n',2,'2017-09-18 06:33:46.276',NULL),(10005,2,3,'crm1.anycapital.com\n',2,'2017-09-18 06:33:46.300',NULL),(10010,1,6,'IL-PC1078\n',2,'2017-11-14 13:06:39.076',NULL),(10010,2,6,'IL-PC1078\n',2,'2017-11-14 13:06:39.598',NULL),(10011,1,6,'IL-PC1078\n',2,'2017-11-14 13:11:07.551',NULL),(10011,2,6,'IL-PC1078\n',2,'2017-11-14 13:11:07.946',NULL);
/*!40000 ALTER TABLE `transactions_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:37
