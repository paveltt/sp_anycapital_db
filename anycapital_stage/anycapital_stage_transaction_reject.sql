-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_reject`
--

DROP TABLE IF EXISTS `transaction_reject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_reject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `transaction_payment_type_id` smallint(6) NOT NULL,
  `transaction_reject_type_id` smallint(6) NOT NULL,
  `transaction_operation_id` tinyint(4) NOT NULL,
  `Amount` int(11) NOT NULL,
  `action_source_id` tinyint(4) NOT NULL,
  `writer_id` smallint(6) DEFAULT NULL,
  `info` varchar(250) DEFAULT NULL,
  `SESSION_ID` varchar(50) DEFAULT NULL,
  `server_name` varchar(50) NOT NULL,
  `time_created` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  KEY `fk_transaction_payment_type_id_idx` (`transaction_payment_type_id`),
  KEY `fk_action_source_id_idx` (`action_source_id`),
  KEY `fk_writer_id_idx` (`writer_id`),
  KEY `fk_transaction_reject_type_id_idx` (`transaction_reject_type_id`),
  KEY `fk_transaction_operation_id_idx` (`transaction_operation_id`),
  CONSTRAINT `fk_action_source_id` FOREIGN KEY (`action_source_id`) REFERENCES `action_source` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_operation_id` FOREIGN KEY (`transaction_operation_id`) REFERENCES `transaction_operations` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_payment_type_id` FOREIGN KEY (`transaction_payment_type_id`) REFERENCES `transaction_payment_types` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_reject_type_id` FOREIGN KEY (`transaction_reject_type_id`) REFERENCES `transaction_reject_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_writer_id` FOREIGN KEY (`writer_id`) REFERENCES `writers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_reject`
--

LOCK TABLES `transaction_reject` WRITE;
/*!40000 ALTER TABLE `transaction_reject` DISABLE KEYS */;
INSERT INTO `transaction_reject` VALUES (1,10007,1,4,2,100000,1,NULL,'info','session','ww00.anycapital.com\n','2017-09-08 05:50:49.068');
/*!40000 ALTER TABLE `transaction_reject` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:54
