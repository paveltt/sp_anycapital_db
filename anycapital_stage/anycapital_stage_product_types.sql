-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_types`
--

DROP TABLE IF EXISTS `product_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_types` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  `DISPLAY_NAME` varchar(20) NOT NULL COMMENT 'messages properties key name',
  `PRODUCT_CATEGORY_ID` smallint(6) NOT NULL,
  `IS_COUPON` binary(1) DEFAULT '0' COMMENT '1 if this category should have coupon option',
  `IS_DAY_COUNT_CONVENTION` binary(1) DEFAULT '0' COMMENT '1 if this category should have DAY COUNT CONVENTION option',
  `IS_MAX_YIELD` binary(1) DEFAULT NULL COMMENT '1 if this category should have Max Yield option',
  `IS_MAX_YIELD_P_A` binary(1) DEFAULT '0' COMMENT '1 if this category should have Max Yield p.a option',
  `IS_PARTICIPATION` binary(1) DEFAULT '0' COMMENT '1 if this category should have PARTICIPATION option',
  `IS_STRIKE_LEVEL` binary(1) DEFAULT '0' COMMENT '1 if this category should have STRIKE LEVEL option',
  `IS_BONUS_PRECENTAGE` binary(1) DEFAULT '0' COMMENT '1 if this category should have BONUS PRECENTAGE option',
  `RISK_LEVEL` tinyint(4) NOT NULL COMMENT 'the risk level of this type',
  `FORMULA_CLASS_PATH` varchar(100) NOT NULL COMMENT 'the class path to the formula in our application',
  `FORMULA_PARAMETERS` varchar(100) NOT NULL COMMENT 'json string with parameters for the formula class',
  `IS_DISTANCE_TO_BARRIER` binary(1) NOT NULL DEFAULT '0' COMMENT '1 if this type can have DISTANCE TO BARRIER else 0',
  `IS_CAP_LEVEL` binary(1) NOT NULL DEFAULT '0' COMMENT '1 if this type can have cap level else 0',
  `PRODUCT_TYPE_P_L_ID` smallint(6) NOT NULL DEFAULT '1' COMMENT 'PRODUCT_TYPE_PROTECTION_LEVEL id',
  `SLIDER_MIN` int(11) NOT NULL DEFAULT '-100' COMMENT 'min value for the slider',
  `SLIDER_MAX` int(11) NOT NULL DEFAULT '100' COMMENT 'max value for the slider',
  `is_Active` binary(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `PRODUCT_CATEGORY_ID` (`PRODUCT_CATEGORY_ID`),
  KEY `PRODUCT_TYPE_P_L_ID` (`PRODUCT_TYPE_P_L_ID`),
  CONSTRAINT `product_types_ibfk_1` FOREIGN KEY (`PRODUCT_CATEGORY_ID`) REFERENCES `product_categories` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `product_types_ibfk_2` FOREIGN KEY (`PRODUCT_TYPE_P_L_ID`) REFERENCES `product_type_protection_level` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COMMENT='all product types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_types`
--

LOCK TABLES `product_types` WRITE;
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;
INSERT INTO `product_types` VALUES (1,'Capital Protection Cartificate with Participation','product.type.1',1,'0','1','1','1','1','0','0',1,'capital.any.product.type.formula.CPCP','{\"scenarioPercentages\": [180, 120, 100]}','0','0',1,-100,100,'1'),(2,'Barrier Capital Protection Certificate','product.type.2',1,'0','1','1','1','1','0','0',1,'capital.any.product.type.formula.BCPC','{\"scenarioPercentages\": [80, 100, 100]}','1','0',1,-100,100,'1'),(3,'Capital Protection Certificate with Coupon','product.type.3',1,'1','1','0','0','0','0','0',1,'capital.any.product.type.formula.CPCC','{\"scenarioPercentages\": [100, 100]}','0','0',1,-100,100,'1'),(4,'Reverse Convertible','product.type.4',2,'1','1','0','0','0','1','0',3,'capital.any.product.type.formula.RC','{\"scenarioPercentages\": [100, 90, 50]}','0','0',2,-100,100,'1'),(5,'Barrier Reverse Convertible','product.type.5',2,'1','1','0','0','0','0','0',2,'capital.any.product.type.formula.BRC','{\"scenarioPercentages\": [110, 80]}','1','0',2,-100,100,'1'),(6,'Inverse Barrier Reverse Convertible','product.type.6',2,'1','1','0','0','0','0','0',2,'capital.any.product.type.formula.IBRC','{\"scenarioPercentages\": [90, 120]}','1','0',2,-100,140,'1'),(7,'Express Certificate','product.type.7',2,'1','1','0','0','0','1','0',2,'capital.any.product.type.formula.EC','{\"scenarioPercentages\": [110, 90, 80]}','1','0',2,-100,100,'1'),(8,'Outperformance Certificate','product.type.8',3,'0','0','0','0','1','0','0',3,'capital.any.product.type.formula.OC','{\"scenarioPercentages\": [160, 110, 80, 120, 50, 80]}','0','1',3,-100,100,'1'),(9,'Bonus Certificate','product.type.9',3,'0','0','1','1','1','1','1',2,'capital.any.product.type.formula.BC','{\"scenarioPercentages\": [140, 90, 80]}','1','0',2,-100,100,'1'),(10,'Bonus Outperformance Certificate','product.type.10',3,'0','0','1','1','1','1','1',2,'capital.any.product.type.formula.BOC','{\"scenarioPercentages\": [150, 80, 80]}','1','0',2,-100,100,'0');
/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:46:33
