-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_simulation`
--

DROP TABLE IF EXISTS `product_simulation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_simulation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FINAL_FIXING_LEVEL` double NOT NULL,
  `POSITION` smallint(6) NOT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `FINAL_FIXING_LEVEL` (`FINAL_FIXING_LEVEL`,`PRODUCT_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`),
  CONSTRAINT `product_simulation_ibfk_1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_simulation`
--

LOCK TABLES `product_simulation` WRITE;
/*!40000 ALTER TABLE `product_simulation` DISABLE KEYS */;
INSERT INTO `product_simulation` VALUES (1,100,1,1),(2,120,2,1),(3,130,3,1),(4,140,1,2),(5,120,2,2),(6,100,3,2),(7,80,4,2),(8,60,5,2),(9,40,6,2),(10,160,1,1000),(11,140,2,1000),(12,120,3,1000),(13,100,4,1000),(14,80,5,1000),(15,60,6,1000),(16,140,1,1001),(17,120,2,1001),(18,100,3,1001),(19,80,4,1001),(20,60,5,1001),(21,40,6,1001),(22,180,1,1002),(23,160,2,1002),(24,140,3,1002),(25,120,4,1002),(26,100,5,1002),(27,80,6,1002),(28,60,7,1002),(29,160,1,1003),(30,150,2,1003),(31,140,3,1003),(32,130,4,1003),(33,120,5,1003),(34,110,6,1003),(35,100,7,1003),(36,90,8,1003),(37,70,9,1003),(38,50,10,1003),(39,180,1,1004),(40,160,2,1004),(41,140,3,1004),(42,120,4,1004),(43,100,5,1004),(44,80,6,1004),(45,60,7,1004),(46,40,8,1004),(47,160,1,1005),(48,140,2,1005),(49,120,3,1005),(50,100,4,1005),(51,80,5,1005),(52,60,6,1005),(53,40,7,1005),(54,160,1,1006),(55,140,2,1006),(56,120,3,1006),(57,100,4,1006),(58,80,5,1006),(59,60,6,1006),(60,40,7,1006),(61,0,1,1007),(62,160,2,1007),(63,140,3,1007),(64,120,4,1007),(65,100,5,1007),(66,80,6,1007),(67,60,7,1007),(68,40,8,1007);
/*!40000 ALTER TABLE `product_simulation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:08
