-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `regulation_questionnaire_status_score`
--

DROP TABLE IF EXISTS `regulation_questionnaire_status_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulation_questionnaire_status_score` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `score` bigint(20) NOT NULL,
  `status_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_regulation_questionnaire_status_id_idx` (`status_id`),
  CONSTRAINT `fk_regulation_questionnaire_status_id` FOREIGN KEY (`status_id`) REFERENCES `regulation_questionnaire_statuses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='The table represent the regulation_questionnaire_status by sum of answer''s score.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regulation_questionnaire_status_score`
--

LOCK TABLES `regulation_questionnaire_status_score` WRITE;
/*!40000 ALTER TABLE `regulation_questionnaire_status_score` DISABLE KEYS */;
INSERT INTO `regulation_questionnaire_status_score` VALUES (1,1111,1),(2,101,4),(3,1001,4),(4,111,4),(5,1011,4),(6,1101,4),(7,1110,4);
/*!40000 ALTER TABLE `regulation_questionnaire_status_score` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:46:02
