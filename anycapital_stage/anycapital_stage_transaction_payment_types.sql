-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_payment_types`
--

DROP TABLE IF EXISTS `transaction_payment_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_payment_types` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL COMMENT 'name of the payment type',
  `TRANSACTION_CLASS_ID` tinyint(4) NOT NULL COMMENT 'reference',
  `DISPLAY_NAME` varchar(30) DEFAULT NULL,
  `OBJECT_CLASS` varchar(50) DEFAULT 'Object.class' COMMENT 'blueprint class path - represent the payment class type',
  `HANDLER_CLASS` varchar(100) DEFAULT 'Object.class' COMMENT 'class path of the class who responsible to handle the payment type',
  PRIMARY KEY (`ID`),
  KEY `TRANSACTION_CLASS_ID` (`TRANSACTION_CLASS_ID`),
  CONSTRAINT `transaction_payment_types_ibfk_1` FOREIGN KEY (`TRANSACTION_CLASS_ID`) REFERENCES `transaction_classes` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_payment_types`
--

LOCK TABLES `transaction_payment_types` WRITE;
/*!40000 ALTER TABLE `transaction_payment_types` DISABLE KEYS */;
INSERT INTO `transaction_payment_types` VALUES (1,'Bank Wire',1,'transaction.payment.type.1','capital.any.model.base.Wire','capital.any.service.base.payment.handler.WirePaymentHandler'),(2,'investment coupon',2,'transaction.payment.type.2','java.lang.Object','java.lang.Object'),(3,'Admin',2,'transaction.payment.type.3','java.lang.Object','capital.any.service.base.payment.handler.AdminPaymentHandler');
/*!40000 ALTER TABLE `transaction_payment_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:02
