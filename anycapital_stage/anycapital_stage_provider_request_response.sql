-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `provider_request_response`
--

DROP TABLE IF EXISTS `provider_request_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_request_response` (
  `TABLE_ID` smallint(6) NOT NULL COMMENT 'Reference to `tables` table',
  `REFERENCE_ID` bigint(20) DEFAULT NULL COMMENT 'the id on the table',
  `REQUEST` longtext COMMENT 'request to provider',
  `RESPONSE` longtext COMMENT 'response to provider',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `TIME_UPDATED` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`TIME_CREATED`),
  KEY `TABLE_ID` (`TABLE_ID`),
  CONSTRAINT `provider_request_response_ibfk_1` FOREIGN KEY (`TABLE_ID`) REFERENCES `tables` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_request_response`
--

LOCK TABLES `provider_request_response` WRITE;
/*!40000 ALTER TABLE `provider_request_response` DISABLE KEYS */;
INSERT INTO `provider_request_response` VALUES (4,1,'xml=%3Cmessage%3E%0A++++%3Coriginator%3Eanycapital%3C%2Foriginator%3E%0A++++%3Crecipient%3E441234567%3C%2Frecipient%3E%0A++++%3Cbody%3EReset+password+link%3A+https%3A%2F%2Fgoo.gl%2F81UWXQ%3C%2Fbody%3E%0A++++%3Creference%3E1%3C%2Freference%3E%0A++++%3CrouteId%3Emhlrglobal%3C%2FrouteId%3E%0A%3C%2Fmessage%3E','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><xaresponse><authentication><code>0</code><text>OK</text></authentication><message><body>Reset password link: https://goo.gl/81UWXQ</body><id>C6C97EA50A931B31090C0251F16DB8D9</id><originator>anycapital</originator><recipient>441234567</recipient><reference>1</reference><routeId>mhlrglobal</routeId></message></xaresponse>','2017-08-09 11:38:00.298',NULL),(4,2,'xml=%3Cmessage%3E%0A++++%3Coriginator%3Eanycapital%3C%2Foriginator%3E%0A++++%3Crecipient%3E441234567%3C%2Frecipient%3E%0A++++%3Cbody%3EReset+password+link%3A+https%3A%2F%2Fgoo.gl%2FBBLuTL%3C%2Fbody%3E%0A++++%3Creference%3E2%3C%2Freference%3E%0A++++%3CrouteId%3Emhlrglobal%3C%2FrouteId%3E%0A%3C%2Fmessage%3E','<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><xaresponse><authentication><code>0</code><text>OK</text></authentication><message><body>Reset password link: https://goo.gl/BBLuTL</body><id>C73586C40A931B31090C02510A707AC9</id><originator>anycapital</originator><recipient>441234567</recipient><reference>2</reference><routeId>mhlrglobal</routeId></message></xaresponse>','2017-08-09 13:36:00.250',NULL);
/*!40000 ALTER TABLE `provider_request_response` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:43
