-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `file_type_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `time_created` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `action_source_id` tinyint(4) NOT NULL,
  `writer_id` smallint(4) DEFAULT NULL COMMENT 'File added by this writer',
  `is_primary` binary(1) DEFAULT '0' COMMENT '1 if this file is the most update file from this type to this user',
  `status_id` smallint(6) NOT NULL DEFAULT '1',
  `updated_by_writer_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_fk_idx` (`user_id`),
  KEY `file_type_fk_idx` (`file_type_id`),
  KEY `action_source_fk_idx` (`action_source_id`),
  KEY `writer_fk_idx` (`writer_id`),
  KEY `updated_by_writer_files_fk_idx` (`updated_by_writer_id`),
  CONSTRAINT `writer_files_fk` FOREIGN KEY (`writer_id`) REFERENCES `writers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `action_source_files_fk` FOREIGN KEY (`action_source_id`) REFERENCES `action_source` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `file_type_files_fk` FOREIGN KEY (`file_type_id`) REFERENCES `file_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `users_files_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (5,'1504770589007_-_mz.jpg',1,4,'2017-09-07 07:49:49.009',2,3,'1',1,NULL),(6,'1504775902494_-_mz.jpg',2,4,'2017-09-07 09:18:22.495',2,3,'1',1,NULL),(7,'1505133571280_-_2017-09-11_1532.png',4,10028,'2017-09-11 12:39:31.283',2,106,'1',1,NULL),(8,'1505133585288_-_user_752848_doc_1_10042014_181905_Digitalizzato a 10-04-2014 10.35 (4).jpg',1,10028,'2017-09-11 12:39:45.290',2,106,'0',1,NULL),(9,'1505308488141_-_zakariya kasu poi 13.9.17.jpg',2,10050,'2017-09-13 13:14:48.141',2,105,'1',1,NULL),(10,'1505308505595_-_zakariya poa 13.9.17.pdf',4,10050,'2017-09-13 13:15:05.595',2,105,'1',1,NULL),(11,'1505370251758_-_IBRAHIM_THIAM_ID_FRONT.png',1,10029,'2017-09-14 06:24:11.761',2,105,'0',1,NULL),(12,'1505370275854_-_IBRAHIM_THIAM_ID_BACK.png',1,10029,'2017-09-14 06:24:35.862',2,105,'0',1,NULL),(13,'1505370299427_-_IBRAHIM_THIAM_UB_EXPIRE_6.3.18.png',4,10029,'2017-09-14 06:24:59.431',2,105,'1',1,NULL),(14,'1506414338040_-_mz.jpg',4,4,'2017-09-26 08:25:38.041',2,3,'1',1,NULL),(19,'aaaaaaa.png',1,10074,'2017-11-14 14:32:32.880',2,3,'0',1,NULL),(24,'1510991500853_-_download.jpg',2,10096,'2017-11-18 07:51:41.016',2,3,'1',1,NULL);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:52
