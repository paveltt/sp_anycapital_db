-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `markets`
--

DROP TABLE IF EXISTS `markets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `markets` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(40) NOT NULL,
  `DISPLAY_NAME` varchar(20) NOT NULL,
  `MARKET_GROUP_ID` smallint(6) NOT NULL,
  `TICKER` varchar(30) NOT NULL,
  `CURRENCY` varchar(20) NOT NULL COMMENT 'market exchange currency',
  `FEED_NAME` varchar(20) NOT NULL,
  `IS_ACTIVE` tinyint(4) DEFAULT '1',
  `DECIMAL_POINT` smallint(6) DEFAULT '2',
  `MARKET_ID_AO` smallint(6) DEFAULT NULL COMMENT 'the market id in ao db',
  `MARKET_PLACE` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MARKET_GROUP_ID` (`MARKET_GROUP_ID`),
  CONSTRAINT `markets_ibfk_1` FOREIGN KEY (`MARKET_GROUP_ID`) REFERENCES `market_groups` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `markets`
--

LOCK TABLES `markets` WRITE;
/*!40000 ALTER TABLE `markets` DISABLE KEYS */;
INSERT INTO `markets` VALUES (1,'S&P 500 Index','market.1',1,'SPX Index','USD','^GSPC',1,2,6,'NYSE'),(2,'FTSE 100 Index','market.2',1,'UKX Index','GBP','^FTSE',1,2,4,'London Stock Exchange'),(3,'Nikkei 225','market.3',1,'NKY Index','JPY','^N225',1,2,9,'Tokyo Stock Exchange'),(4,'DAX Index','market.4',1,'DAX Index','EUR','^GDAXI',1,2,8,'Xetra'),(5,'CAC 40 Index','market.5',1,'CAC Index','EUR','^FCHI',1,2,10,'Euronext Paris'),(6,'Dow Jones Indus Avg','market.6',1,'Indu Index','USD','^DJI',1,2,5,'NYSE'),(7,'Nasdaq 100 Composite Index','market.7',1,'NDX Index','USD','^IXIC',1,2,7,'NASDAQ'),(8,'Apple','market.8',2,'AAPL UQ Equity','USD','AAPL',1,2,129,'NASDAQ'),(9,'Alibaba','market.9',2,'BABA UN Equity','USD','BABA',1,2,679,'NYSE'),(10,'Amazon','market.10',2,'AMZN UQ Equity','USD','AMZN',1,2,627,'Nasdaq'),(11,'Twitter','market.11',2,'TWTR UN Equity','USD','TWTR',1,2,648,'NYSE'),(12,'Facebook','market.12',2,'FB UQ Equity','USD','FB',1,2,620,'Nasdaq'),(13,'Deutsche Bank','market.13',2,'DBK GY Equity','EUR','DBK.DE',1,2,394,'Xetra'),(14,'Allianz','market.14',2,'ALV GY Equity','EUR','ALV.DE',1,2,537,'Xetra'),(15,'BMW','market.15',2,'BMW GY Equity','EUR','BMW.DE',1,2,702,'Xetra'),(16,'Microsoft','market.16',2,'MSFT UQ Equity','USD','MSFT',1,2,130,'NASDAQ'),(17,'Adidas','market.17',2,'ADS GY Equity','EUR','ADS.DE',1,2,697,'Xetra'),(18,'Coca-Cola','market.18',2,'KO US Equity','USD','KO',1,2,694,'NYSE'),(19,'Cisco','market.19',2,'CSCO US Equity','USD','CSCO',1,2,626,'NASDAQ'),(20,'Paypal','market.20',2,'PYPL UQ Equity','USD','PYPL',1,2,708,'NASDAQ'),(21,'Citi Group','market.21',2,'C UN Equity','USD','C',1,2,128,'NYSE'),(22,'Bank Of America','market.22',2,'BAC US Equity','USD','BAC',1,2,598,'NYSE'),(23,'Goldman Sachs','market.23',2,'GS UN Equity','USD','GS',1,2,591,'NYSE'),(24,'Pfizer','market.24',2,'PFE UN Equity','USD','PFE',1,2,628,'NYSE'),(25,'Procter&gamble','market.25',2,'PG UN Equity','USD','PG',1,2,713,'NYSE'),(26,'AIG','market.26',2,'AIG UN Equity','USD','AIG',1,2,629,'NYSE'),(27,'Wal-mart stores','market.27',2,'WMT US Equity','USD','WMT',1,2,769,'NYSE'),(28,'Tesla','market.28',2,'TSLA US Equity','USD','TSLA',1,2,767,'NASDAQ'),(29,'Google','market.29',2,'GOOG US Equity','USD','GOOG',1,2,132,'NASDAQ'),(30,'British Petroleum','market.30',2,'BP/ LN Equity','GBP','BP.L',1,2,415,'London Stock Exchange'),(31,'Royal Bank of Scotland','market.31',2,'RBS LN Equity','GBP','RBS.L',1,2,433,'London Stock Exchange'),(32,'Banco Santander','market.32',2,'SAN SM Equity','EUR','SAN.MC',1,2,543,'Madrid Stock Exchange'),(33,'Exxon Mobil','market.33',2,'XOM US Equity','USD','XOM',1,2,565,'NYSE'),(34,'Societe Generale','market.34',2,'GLE FP Equity','EUR','GLE.PA',1,2,587,'Euronext Paris'),(35,'NIKE','market.35',2,'NKE US Equity','USD','NKE',1,2,696,'NYSE'),(36,'Ebay','market.36',2,'EBAY US Equity','USD','EBAY',1,2,770,'NASDAQ'),(37,'General Motors','market.37',2,'GM US Equity','USD','GM',1,2,761,'NYSE'),(38,'Delta Airlines','market.38',2,'DAL US Equity','USD','DAL',1,2,786,'NYSE'),(39,'EUR/USD','market.39',5,'EURUSD CURNCY','EUR','EURUSD=X',1,4,16,'OTC');
/*!40000 ALTER TABLE `markets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:51
