-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `marketing_contents`
--

DROP TABLE IF EXISTS `marketing_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketing_contents` (
  `ID` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Content''s id',
  `NAME` varchar(50) NOT NULL COMMENT 'Content''s name',
  `COMMENTS` varchar(4000) DEFAULT NULL COMMENT 'Any comments',
  `PAGE_CONTENT` longtext COMMENT 'Page content can include html file etc.',
  `WRITER_ID` smallint(6) NOT NULL COMMENT 'Foreign key to WRITERS',
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'Content''s time created',
  `TIME_MODIFIED` timestamp NULL DEFAULT NULL COMMENT 'Content''s time updated',
  `HTML_FILE_PATH` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  CONSTRAINT `marketing_contents_ibfk_1` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marketing_contents`
--

LOCK TABLES `marketing_contents` WRITE;
/*!40000 ALTER TABLE `marketing_contents` DISABLE KEYS */;
INSERT INTO `marketing_contents` VALUES (1,'default',NULL,NULL,3,'2017-08-03 16:19:04.908',NULL,NULL),(2,'anycapital_mailer_v_e',NULL,NULL,104,'2017-09-06 08:27:09.214',NULL,NULL),(3,'anycapital_mailer_v_a',NULL,NULL,104,'2017-09-06 08:30:46.641',NULL,NULL),(4,'anycapital_mailer_v_b',NULL,NULL,3,'2017-09-13 06:35:24.057',NULL,NULL),(5,'anycapital_mailer_v_c',NULL,NULL,3,'2017-09-13 06:36:19.756',NULL,NULL),(6,'AO_Popup','All_login_Clients',NULL,104,'2017-09-25 09:31:48.775',NULL,NULL),(7,'Woman_SG_Email',NULL,NULL,3,'2017-09-27 07:31:18.978','2017-11-18 07:21:03','7_-_44_-_1.html'),(8,'SMS_promotion_AO_users_EN','Hey, Have you seen our Capital notes?  You can benefit from 200% participation on Tesla’s price increase, and never lose more than 5% if you hold the product until maturity.  Register Now!',NULL,3,'2017-10-02 09:31:49.636','2017-10-17 11:19:54',NULL),(9,'SMS_promotion_AO_users_ES','Hola ¿ha visto nuestras notas de capital?  Puede beneficiarse del 200% de participación en el aumento de precios de Tesla y nunca perder más del 5% si mantiene el producto hasta el vencimiento.  ¡Regístrate ahora!',NULL,3,'2017-10-17 11:20:25.581',NULL,NULL),(10,'SMS_promotion_AO_users_DE','Haben Sie unsere Capital Notizen gesehen?   Sie können von 200% Anteil von Tesla\'s Preissteigerung profitieren und nie mehr als 5% verlieren, wenn Sie diesen Artikel bis zur Fälligkeit behalten.  Jetzt anmelden',NULL,3,'2017-10-17 11:21:11.061',NULL,NULL);
/*!40000 ALTER TABLE `marketing_contents` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:46:37
