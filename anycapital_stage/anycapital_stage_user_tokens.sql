-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_tokens`
--

DROP TABLE IF EXISTS `user_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tokens` (
  `USER_ID` bigint(20) NOT NULL,
  `TOKEN` varchar(256) NOT NULL,
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='user tokens. first stage - using for change password. second stage - may add types to use also other user token types.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tokens`
--

LOCK TABLES `user_tokens` WRITE;
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;
INSERT INTO `user_tokens` VALUES (3,'15019343415803','2017-08-05 11:59:01.582'),(4,'15021766968344','2017-08-08 07:18:16.838'),(4,'15021777768074','2017-08-08 07:36:16.807'),(4,'15021787286234','2017-08-08 07:52:08.630'),(4,'15021814528904','2017-08-08 08:37:32.897'),(4,'15022784691384','2017-08-09 11:34:29.135'),(4,'15022786714194','2017-08-09 11:37:51.415'),(4,'15022848355454','2017-08-09 13:20:35.545'),(4,'15022857534424','2017-08-09 13:35:53.441'),(4,'15032142310304','2017-08-20 07:30:31.036'),(4,'15044269999834','2017-09-03 08:23:19.991'),(4,'15045972637974','2017-09-05 07:41:03.803'),(4,'15045973410704','2017-09-05 07:42:21.078'),(4,'15047667412914','2017-09-07 06:45:41.296'),(4,'15057161404464','2017-09-18 06:29:00.450'),(10007,'150581767329110007','2017-09-19 10:41:13.296'),(10059,'150591583159710059','2017-09-20 13:57:11.598'),(4,'15062546811334','2017-09-24 12:04:41.134');
/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:44:30
