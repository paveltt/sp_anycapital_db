-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_coupons`
--

DROP TABLE IF EXISTS `product_coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_coupons` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` bigint(20) NOT NULL,
  `OBSERVATION_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'on which date to check the coupon trigger level',
  `PAYMENT_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'on which date to pay coupon',
  `TRIGGER_LEVEL` double NOT NULL COMMENT 'percentage to check from start price of market that done worst performance',
  `PAY_RATE` double NOT NULL COMMENT 'percentage from denomination to pay customer',
  `IS_PAID` binary(1) NOT NULL DEFAULT '0' COMMENT '1 if we paid this coupon',
  `OBSERVATION_LEVEL` double DEFAULT NULL COMMENT 'the level to use in the observation date',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PRODUCT_ID` (`PRODUCT_ID`,`OBSERVATION_DATE`),
  CONSTRAINT `product_coupons_ibfk_1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='pay coupon at payment date if in the OBSERVATION DATE the condition was true';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_coupons`
--

LOCK TABLES `product_coupons` WRITE;
/*!40000 ALTER TABLE `product_coupons` DISABLE KEYS */;
INSERT INTO `product_coupons` VALUES (1,2,'2018-08-27 00:00:00','2018-08-29 00:00:00',0,8.5,'0',NULL),(2,1001,'2019-09-28 21:00:00','2019-09-30 21:00:00',0,4.5,'0',NULL),(3,1005,'2018-11-12 22:00:00','2018-11-14 22:00:00',0,5.5,'0',NULL),(4,1006,'2018-12-13 22:00:00','2018-12-17 22:00:00',0,5.5,'0',NULL),(5,1007,'2020-12-10 22:00:00','2020-12-15 22:00:00',0,5.5,'0',NULL);
/*!40000 ALTER TABLE `product_coupons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:45:21
