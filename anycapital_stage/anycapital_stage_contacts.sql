-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: testdb1.anycapital.com    Database: anycapital_stage
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `LAST_NAME` varchar(100) DEFAULT NULL,
  `TYPE_ID` tinyint(4) NOT NULL,
  `MOBILE_PHONE` varchar(30) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `COUNTRY_ID` smallint(6) NOT NULL,
  `LANGUAGE_ID` smallint(6) NOT NULL,
  `TIME_CREATED` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `TIME_MODIFIED` timestamp NULL DEFAULT NULL,
  `WRITER_ID` smallint(6) DEFAULT NULL,
  `UTC_OFFSET` smallint(6) DEFAULT NULL,
  `IP` varchar(20) DEFAULT NULL,
  `USER_AGENT` varchar(2000) DEFAULT NULL,
  `IS_CONTACT_BY_EMAIL` binary(1) DEFAULT '1',
  `IS_CONTACT_BY_SMS` binary(1) DEFAULT '1',
  `CLASS_ID` tinyint(4) DEFAULT '1',
  `ACTION_SOURCE_ID` tinyint(4) NOT NULL,
  `HTTP_REFERER` varchar(4000) DEFAULT NULL,
  `ISSUE_ID` tinyint(4) DEFAULT NULL,
  `COMMENTS` varchar(4000) DEFAULT NULL,
  `MARKETING_TRACKING_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ISSUE_ID` (`ISSUE_ID`),
  KEY `COUNTRY_ID` (`COUNTRY_ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  KEY `CLASS_ID` (`CLASS_ID`),
  KEY `WRITER_ID` (`WRITER_ID`),
  KEY `ACTION_SOURCE_ID` (`ACTION_SOURCE_ID`),
  KEY `contacts_ibfk_7_idx` (`LANGUAGE_ID`),
  KEY `contacts_ibfk_7_idx1` (`MARKETING_TRACKING_ID`),
  CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`ISSUE_ID`) REFERENCES `contact_issues` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `countries` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_3` FOREIGN KEY (`TYPE_ID`) REFERENCES `contact_types` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_4` FOREIGN KEY (`CLASS_ID`) REFERENCES `user_classes` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_5` FOREIGN KEY (`WRITER_ID`) REFERENCES `writers` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_6` FOREIGN KEY (`ACTION_SOURCE_ID`) REFERENCES `action_source` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `contacts_ibfk_7` FOREIGN KEY (`LANGUAGE_ID`) REFERENCES `languages` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10011 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (10000,'asd','asd',2,'213234234234','zubi@anycapital.com',226,2,'2017-08-09 13:31:29.051',NULL,NULL,0,'31.154.37.138','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36','1','1',1,1,NULL,2,'1=1',NULL),(10001,'Neil','Bacon',2,'2036184911','neil.bacon@aol.co.uk',226,2,'2017-09-11 07:25:44.134',NULL,NULL,0,'86.23.68.120','Mozilla/5.0 (Windows NT 5.1; rv:50.0) Gecko/20100101 Firefox/50.0','1','1',1,1,NULL,2,'Hi! My name is Neil Bacon. My account email is neil.bacon@aol.co.uk.\n\nI am trying to find a really good broker which can satisfy all my needs as a trader. I am going to trade by myself.\nI don\'t need a',NULL),(10002,'Jess','Jimenez',2,'645628299','reformas7@hotmail.com',15,2,'2017-09-15 08:59:58.164',NULL,NULL,0,'47.60.162.39','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36','1','1',1,1,NULL,2,'Buenas, para que me llamé un comercial lo antes posible. Gracias',NULL);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-05 17:44:52
